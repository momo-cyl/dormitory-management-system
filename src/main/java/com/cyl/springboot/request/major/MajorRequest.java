package com.cyl.springboot.request.major;

import com.cyl.springboot.request.PageRequest;
import lombok.Data;

import java.util.List;


@Data
public class MajorRequest extends PageRequest {

    //学院名称(模糊查询)
    private String collegeNameLike;

    private String majorNameLike;

    private List<Long> idList;

    private Boolean delFlag;
}
