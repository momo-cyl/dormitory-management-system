package com.cyl.springboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Location;
import com.cyl.springboot.request.LocationRequest;
import com.cyl.springboot.response.LocationResponse;

public interface LocationService extends IService<Location> {

    Page<LocationResponse> queryPage(LocationRequest request);


}
