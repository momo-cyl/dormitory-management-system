package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.College;

public interface CollegeMapper extends BaseMapper<College> {
}
