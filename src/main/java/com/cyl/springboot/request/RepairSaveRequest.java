package com.cyl.springboot.request;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * 维修表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
public class RepairSaveRequest {

    //ID
    private Integer id;

    //业务主键id
    private String repairId;

    //用户id
    private String userId;

    //宿舍id
    private String dormitoryId;

    //状态
    private String status;

    //描述
    private String description;

    //文件id列表
    private List<String> fileIdList;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
