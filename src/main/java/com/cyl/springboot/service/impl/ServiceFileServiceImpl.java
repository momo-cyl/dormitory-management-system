package com.cyl.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.ServiceFile;
import com.cyl.springboot.mapper.ServiceFileMapper;
import com.cyl.springboot.service.ServiceFileService;
import org.springframework.stereotype.Service;

@Service
public class ServiceFileServiceImpl extends ServiceImpl<ServiceFileMapper, ServiceFile> implements ServiceFileService {


}
