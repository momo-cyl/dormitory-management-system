package com.cyl.springboot.request.college;

import com.cyl.springboot.request.PageRequest;
import lombok.Data;

import java.util.List;


@Data
public class CollegeRequest extends PageRequest {

    //学院名称(模糊查询)
    private String nameLike;

    private List<Long> idList;
}
