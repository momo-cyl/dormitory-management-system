package com.cyl.springboot.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Location;
import com.cyl.springboot.mapper.LocationMapper;
import com.cyl.springboot.request.LocationRequest;
import com.cyl.springboot.response.LocationResponse;
import com.cyl.springboot.service.LocationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LocationServiceImpl extends ServiceImpl<LocationMapper, Location> implements LocationService {

    @Resource
    private LocationMapper locationMapper;

    @Override
    public Page<LocationResponse> queryPage(LocationRequest request) {
        Page page = new Page(request.getPageNo(), request.getPageSize());
        return locationMapper.queryPage(page, request);
    }
}
