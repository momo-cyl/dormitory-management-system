package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Classroom;
import com.cyl.springboot.request.classroom.ClassroomRequest;
import org.apache.ibatis.annotations.Param;

public interface ClassroomMapper extends BaseMapper<Classroom> {

    Page<Classroom> queryPage(Page<Classroom> page, @Param("request") ClassroomRequest request);
}
