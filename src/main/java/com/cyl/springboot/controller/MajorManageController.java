package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Major;
import com.cyl.springboot.request.major.MajorRequest;
import com.cyl.springboot.request.major.MajorSaveRequest;
import com.cyl.springboot.request.major.MajorUpdateRequest;
import com.cyl.springboot.service.MajorService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/majorManage")
public class MajorManageController {

    @Resource
    private MajorService majorService;

    @GetMapping("/get/{id}")
    public Major getById(@PathVariable("id") Long id) {
        return majorService.getById(id);
    }

    @PostMapping("/queryPage")
    @ResponseBody
    public Page<Major> queryPage(@RequestBody MajorRequest majorRequest) {
        return majorService.queryPage(majorRequest);
    }

    @PostMapping("/save")
    public Boolean save(@RequestBody MajorSaveRequest majorSaveRequest) {
        return majorService.save(majorSaveRequest);
    }

    @PostMapping("/update")
    public Boolean update(@RequestBody MajorUpdateRequest majorUpdateRequest) {
        return majorService.update(majorUpdateRequest);
    }

    @PostMapping("/delete")
    public Boolean delete(@RequestBody MajorRequest majorRequest) {
        return majorService.deleteByIdList(majorRequest.getIdList());
    }
}
