Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		info: {}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		let that = this;
		console.log(options);
		let result = {};
		let arr = ['男', '女'];
		result.sexShow = arr;
		result.visitDate = "请选择";
		console.log(result)
		that.setData({
			info: result,
		});
	},
	bindKeyInput: function(e) {
		this.data.info[`${e.currentTarget.dataset.params}`] = e.detail.value
		this.setData({
			info: this.data.info
		})
		console.log(`info对象：`, this.data.info)
	},
	formSubmit: function(e) {
		const that = this;
		let request = this.data.info;
		let oldDate = request.visitDate;
		let date = {};
		if(request.visitDate != "请选择"){
			date = new Date(oldDate);
			request.visitDate = date;
		}else{
			request.visitDate = null;
		}
		if(request.name == null){
			wx.showToast({
				title: '请输入姓名',
				icon: 'none'
			})
			return;
		}
		if(request.sex == null){
			wx.showToast({
				title: '请输入性别',
				icon: 'none'
			})
			return;
		}
		if(request.age == null){
			wx.showToast({
				title: '请输入年龄',
				icon: 'none'
			})
			return;
		}
		if(request.phone == null){
			wx.showToast({
				title: '请输入手机号',
				icon: 'none'
			})
			return;
		}
		if(request.destination == null){
			wx.showToast({
				title: '请输入目的地',
				icon: 'none'
			})
			return;
		}
		console.log(request)
		wx.request({
			url: `http://114.116.117.208:9090/visit/save`,
			method: 'POST',
			data: {
				name: request.name,
				sex: request.sex,
				age: request.age,
				phone: request.phone,
				destination: request.destination,
				visitDate: request.visitDate
			},
			success: function(res) {
				if (res.data.code === '200') {
					let info = that.data.info;
					info.visitDate = oldDate;
					that.setData({
						info: info
					});
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					});
				}
			}
		})
	},
	sexChange(e) {
		let result = this.data.info;
		result.sex = result.sexShow[e.detail.value];
		this.setData({
			info: result
		});
	},
	dateChange(e) {
		let result = this.data.info;
		console.log(e.detail.value);
		result.visitDate = e.detail.value;
		this.setData({
			info: result
		});
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
