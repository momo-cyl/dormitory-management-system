package com.cyl.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Files;
import com.cyl.springboot.mapper.FileMapper;
import com.cyl.springboot.service.FileService;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, Files> implements FileService {


}
