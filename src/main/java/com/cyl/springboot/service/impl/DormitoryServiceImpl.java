package com.cyl.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Dormitory;
import com.cyl.springboot.entity.DormitoryUser;
import com.cyl.springboot.entity.Files;
import com.cyl.springboot.entity.ServiceFile;
import com.cyl.springboot.mapper.DormitoryMapper;
import com.cyl.springboot.request.DormitoryDeleteRequest;
import com.cyl.springboot.request.DormitoryRequest;
import com.cyl.springboot.request.DormitorySaveRequest;
import com.cyl.springboot.request.DormitoryUserRequest;
import com.cyl.springboot.response.DormitoryResponse;
import com.cyl.springboot.service.DormitoryService;
import com.cyl.springboot.service.DormitoryUserService;
import com.cyl.springboot.service.FileService;
import com.cyl.springboot.service.ServiceFileService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DormitoryServiceImpl extends ServiceImpl<DormitoryMapper, Dormitory> implements DormitoryService {

    @Resource
    private DormitoryUserService dormitoryUserService;

    @Resource
    private ServiceFileService serviceFileService;

    @Resource
    private FileService fileService;


    @Override
    public Page<DormitoryResponse> queryPage(DormitoryRequest request) {
        Page page = new Page(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<Dormitory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getDormitoryNumber())) {
            lambdaQueryWrapper.like(Dormitory::getDormitoryNumber, request.getDormitoryNumber());
        }
        if (StrUtil.isNotBlank(request.getDormitoryBuilding())) {
            lambdaQueryWrapper.like(Dormitory::getDormitoryBuilding, request.getDormitoryBuilding());
        }
        lambdaQueryWrapper.orderByDesc(Dormitory::getCreateTime);
        Page result = this.page(page, lambdaQueryWrapper);
        List<Dormitory> records = result.getRecords();
        List<DormitoryResponse> dormitoryResponseList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(records)) {
            for (Dormitory record : records) {
                DormitoryResponse response = new DormitoryResponse();
                BeanUtil.copyProperties(record, response, false);
                LambdaQueryWrapper<ServiceFile> serviceFileLambdaQueryWrapper = new LambdaQueryWrapper<>();
                serviceFileLambdaQueryWrapper.eq(ServiceFile::getServiceId, record.getDormitoryId());
                List<ServiceFile> list = serviceFileService.list(serviceFileLambdaQueryWrapper);
                if (CollectionUtils.isNotEmpty(list)) {
                    LambdaQueryWrapper<Files> queryWrapper = new LambdaQueryWrapper<>();
                    List<String> fileIdList = new ArrayList<>();
                    for (ServiceFile serviceFile : list) {
                        fileIdList.add(serviceFile.getFileId());
                    }
                    queryWrapper.in(Files::getFileId, fileIdList);
                    List<Files> filesList = fileService.list(queryWrapper);
                    if (CollectionUtils.isNotEmpty(filesList)) {
                        List<String> urlList = new ArrayList<>();
                        for (Files files : filesList) {
                            urlList.add(files.getUrl());
                        }
                        response.setUrlList(urlList);
                        response.setFilesList(filesList);
                    }
                }
                dormitoryResponseList.add(response);
            }
        }
        result.setRecords(dormitoryResponseList);
        return result;
    }

    @Override
    public boolean save(DormitorySaveRequest dormitorySaveRequest) {
        Dormitory dormitory = new Dormitory();
        BeanUtil.copyProperties(dormitorySaveRequest, dormitory, false);
        String dormitoryId = IdUtil.fastSimpleUUID();
        dormitory.setDormitoryId(dormitoryId);
        save(dormitory);
        if (CollectionUtils.isNotEmpty(dormitorySaveRequest.getUserIdList())) {
            List<DormitoryUser> list = new ArrayList<>();
            for (String userId : dormitorySaveRequest.getUserIdList()) {
                DormitoryUser dormitoryUser = new DormitoryUser();
                dormitoryUser.setUserId(userId);
                dormitoryUser.setDormitoryId(dormitoryId);
                list.add(dormitoryUser);
            }
            dormitoryUserService.saveBatch(list);
        }
        saveServiceFile(dormitorySaveRequest, dormitoryId);
        return true;
    }

    @Override
    public boolean update(DormitorySaveRequest dormitorySaveRequest) {
        Dormitory dormitory = new Dormitory();
        BeanUtil.copyProperties(dormitorySaveRequest, dormitory, false);
        LambdaQueryWrapper<Dormitory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Dormitory::getDormitoryId, dormitory.getDormitoryId());
        this.update(dormitory, lambdaQueryWrapper);
        DormitoryUserRequest request = new DormitoryUserRequest();
        request.setDormitoryId(dormitorySaveRequest.getDormitoryId());
        dormitoryUserService.delete(request);
        if (CollectionUtils.isNotEmpty(dormitorySaveRequest.getUserIdList())) {
            List<DormitoryUser> list = new ArrayList<>();
            for (String userId : dormitorySaveRequest.getUserIdList()) {
                DormitoryUser dormitoryUser = new DormitoryUser();
                dormitoryUser.setUserId(userId);
                dormitoryUser.setDormitoryId(dormitorySaveRequest.getDormitoryId());
                list.add(dormitoryUser);
            }
            dormitoryUserService.saveBatch(list);
        }
        LambdaQueryWrapper<ServiceFile> serviceFileLambdaQueryWrapper = new LambdaQueryWrapper<>();
        serviceFileLambdaQueryWrapper.eq(ServiceFile::getServiceId, dormitorySaveRequest.getDormitoryId());
        serviceFileService.remove(serviceFileLambdaQueryWrapper);
        saveServiceFile(dormitorySaveRequest, dormitorySaveRequest.getDormitoryId());
        return true;
    }

    @Override
    public boolean delete(DormitoryDeleteRequest deleteRequest) {
        LambdaQueryWrapper<Dormitory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Dormitory::getDormitoryId, deleteRequest.getDormitoryId());
        this.remove(lambdaQueryWrapper);
        LambdaQueryWrapper<DormitoryUser> dormitoryUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dormitoryUserLambdaQueryWrapper.eq(DormitoryUser::getDormitoryId, deleteRequest.getDormitoryId());
        dormitoryUserService.remove(dormitoryUserLambdaQueryWrapper);
        return true;
    }

    @Override
    public boolean batchDelete(DormitoryDeleteRequest deleteRequest) {
        LambdaQueryWrapper<Dormitory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(Dormitory::getDormitoryId, deleteRequest.getDormitoryIdList());
        this.remove(lambdaQueryWrapper);
        LambdaQueryWrapper<DormitoryUser> dormitoryUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dormitoryUserLambdaQueryWrapper.in(DormitoryUser::getDormitoryId, deleteRequest.getDormitoryIdList());
        dormitoryUserService.remove(dormitoryUserLambdaQueryWrapper);
        return true;
    }

    private void saveServiceFile(DormitorySaveRequest request, String serviceId) {
        if (CollectionUtils.isNotEmpty(request.getFileIdList())) {
            List<ServiceFile> list = new ArrayList<>();
            for (String fileId : request.getFileIdList()) {
                ServiceFile serviceFile = new ServiceFile();
                serviceFile.setFileId(fileId);
                serviceFile.setServiceId(serviceId);
                list.add(serviceFile);
            }
            serviceFileService.saveBatch(list);
        }
    }


}
