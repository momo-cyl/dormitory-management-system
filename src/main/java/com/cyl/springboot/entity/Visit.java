package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("visit")
public class Visit {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    //姓名
    private String name;

    //性别
    private String sex;

    //年龄
    private String age;

    //手机号
    private String phone;

    //目的地
    private String destination;

    //到访日期
    @JsonFormat(timezone = "GMT+8")
    private LocalDateTime visitDate;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
