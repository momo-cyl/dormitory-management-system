package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.common.Result;
import com.cyl.springboot.entity.DormitoryUser;
import com.cyl.springboot.request.DormitoryUserRequest;
import com.cyl.springboot.response.DormitoryUserResponse;
import com.cyl.springboot.service.DormitoryUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/dormitoryUser")
public class DormitoryUserController {

    @Resource
    private DormitoryUserService dormitoryUserService;

    /**
     * 根据ID查询站点信息
     *
     * @param id 主键
     * @return
     */
    @GetMapping("/get/{id}")
    public DormitoryUser getDormitoryUser(@PathVariable("id") Integer id) {
        return dormitoryUserService.getById(id);
    }

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    @PostMapping("/queryPage")
    @ResponseBody
    public Page<DormitoryUser> queryPage(@RequestBody DormitoryUserRequest request) {
        return dormitoryUserService.queryPage(request);
    }

    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    @PostMapping("/getDormitoryUserList")
    @ResponseBody
    public List<DormitoryUserResponse> getDormitoryUserList(@RequestBody DormitoryUserRequest request) {
        return dormitoryUserService.getDormitoryUserList(request);
    }


    /**
     * 新增或更新
     *
     * @param dormitoryUser 站点实体
     * @return
     */
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdateDormitory(@RequestBody DormitoryUser dormitoryUser) {

        if (!dormitoryUserService.saveOrUpdate(dormitoryUser)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键ID删除
     *
     * @param id 主键ID
     * @return
     */
    @PostMapping("/delete/{id}")
    public Result deleteDormitory(@PathVariable("id") Integer id) {
        if (!dormitoryUserService.removeById(id)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键集合批量删除
     *
     * @param ids 主键集合
     * @return
     */
    @PostMapping("/del/batch")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        dormitoryUserService.removeByIds(ids);
        return Result.success();
    }

}
