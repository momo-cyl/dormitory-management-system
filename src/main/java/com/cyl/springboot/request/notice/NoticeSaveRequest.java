package com.cyl.springboot.request.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class NoticeSaveRequest {


    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("内容")
    private String content;

}
