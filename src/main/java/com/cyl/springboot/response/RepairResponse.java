package com.cyl.springboot.response;

import com.cyl.springboot.entity.Files;
import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class RepairResponse {

    //ID
    private Integer id;


    //业务主键id
    private String repairId;

    //宿舍id
    private String dormitoryId;

    //宿舍编号
    private String dormitoryNumber;

    //宿舍楼栋
    private String dormitoryBuilding;

    //存储图片
    private List<Files> filesList;

    //图片url
    private List<String> urlList;

    //状态
    private String status;

    //用户id
    private String userId;

    //用户名
    private String userName;

    //手机号
    private String phone;

    //描述
    private String description;


}
