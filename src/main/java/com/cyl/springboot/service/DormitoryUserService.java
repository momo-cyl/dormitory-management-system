package com.cyl.springboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.DormitoryUser;
import com.cyl.springboot.request.DormitoryUserRequest;
import com.cyl.springboot.response.DormitoryUserResponse;

import java.util.List;

public interface DormitoryUserService extends IService<DormitoryUser> {

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    Page<DormitoryUser> queryPage(DormitoryUserRequest request);

    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    List<DormitoryUser> getList(DormitoryUserRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    boolean delete(DormitoryUserRequest request);


    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    List<DormitoryUserResponse> getDormitoryUserList(DormitoryUserRequest request);


}
