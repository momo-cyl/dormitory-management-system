package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Location;
import com.cyl.springboot.request.LocationRequest;
import com.cyl.springboot.request.PageRequest;
import com.cyl.springboot.response.LocationResponse;
import org.apache.ibatis.annotations.Param;

public interface LocationMapper extends BaseMapper<Location> {

    /**
     * 分页查询
     *
     * @return
     */
    Page<LocationResponse> queryPage(Page<PageRequest> page, @Param("request") LocationRequest request);


}
