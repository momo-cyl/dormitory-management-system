// index.js
// 获取应用实例
const app = getApp()

Page({
		data: {
			motto: 'Hello World',
			userInfo: {},
			hasUserInfo: false,
			username: "",
			hasEdusysStorage: false,
			canIUse: wx.canIUse('button.open-type.getUserInfo'),
			canIUseGetUserProfile: false,
			canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse(
				'open-data.type.userNickName') // 如需尝试获取用户信息可改为false
		},
		// 事件处理函数
		bindViewTap() {
			wx.navigateTo({
				url: '../logs/logs'
			})
		},
		onLoad() {
			if (wx.getUserProfile) {
				this.setData({
					canIUseGetUserProfile: true
				})
			}
			const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
			console.log(edusysUserInfo.id != null)
			if (edusysUserInfo.id != null) {
				this.setData({
					hasEdusysStorage: true,
					username: edusysUserInfo.nickname
				})
			}
		},
		logout: function() {
			try {
				wx.clearStorageSync()
				this.setData({
					username: ''
				})
				wx.navigateTo({
					url: "login"
				})
			} catch (error) {
				wx.showToast({
					title: '退出失败',
					icon: 'none'
				})
			}
		},
		onShow() {
			try {
				const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
				if (edusysUserInfo.id != null) {
					this.setData({
						hasEdusysStorage: true,
						username: edusysUserInfo.nickname
					})
				}
			} catch (error) {}
		},
		getUserProfile(e) {
			// 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
			wx.getUserProfile({
				desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
				success: (res) => {
					console.log(res)
					this.setData({
						userInfo: res.userInfo,
						hasUserInfo: true
					})
				}
			})
		},
		goToPage2: function() {
			try {
				const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
				if (edusysUserInfo.id != null) {
					wx.navigateTo({
						url: "../xinxi/pinformaion"
					})
				}
			} catch (error) {
				wx.showToast({
					title: '您未登录',
					icon: 'none'
				})
			}
		},
		goToPage1: function() {
			const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
			if (edusysUserInfo.id != null) {
				wx.showToast({
					title: '您已登录',
					icon: 'none'
				})
			} else {
				wx.navigateTo({
					url: "login"
				})
			}
		},
		getUserInfo(e) {
			// 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
			console.log(e)
			this.setData({
				userInfo: e.detail.userInfo,
				hasUserInfo: true
			})
		}
	},



)
