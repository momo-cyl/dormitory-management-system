package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.common.Result;
import com.cyl.springboot.entity.Location;
import com.cyl.springboot.request.LocationRequest;
import com.cyl.springboot.request.VisitDeleteRequest;
import com.cyl.springboot.response.LocationResponse;
import com.cyl.springboot.service.LocationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/location")
public class LocationController {

    @Resource
    private LocationService locationService;


    /**
     * @param id 主键
     * @return
     */
    @GetMapping("/get/{id}")
    public Location getVisit(@PathVariable("id") Integer id) {
        return locationService.getById(id);
    }

    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    @PostMapping("/queryPage")
    public Page<LocationResponse> queryPage(@RequestBody LocationRequest request) {
        return locationService.queryPage(request);
    }


    /**
     * 新增
     *
     * @param location
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody Location location) {
        if (!locationService.save(location)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 更新
     *
     * @param location
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody Location location) {
        if (!locationService.updateById(location)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键ID删除
     *
     * @param request 主键ID
     * @return
     */
    @PostMapping("/delete")
    public Result deleteRepair(@RequestBody VisitDeleteRequest request) {
        if (!locationService.removeById(request.getId())) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键集合批量删除
     *
     * @param request 主键集合
     * @return
     */
    @PostMapping("/delete/batch")
    public Result deleteBatch(@RequestBody VisitDeleteRequest request) {
        if (!locationService.removeByIds(request.getIds())) {
            return Result.error();
        }
        return Result.success();
    }

}
