package com.cyl.springboot.request;

import lombok.Data;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class PageRequest {

    //页码
    private Integer pageNo;

    //页数
    private Integer pageSize;

}
