package com.cyl.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Files;
import com.cyl.springboot.entity.Repair;
import com.cyl.springboot.entity.ServiceFile;
import com.cyl.springboot.mapper.RepairMapper;
import com.cyl.springboot.request.RepairRequest;
import com.cyl.springboot.request.RepairSaveRequest;
import com.cyl.springboot.response.RepairResponse;
import com.cyl.springboot.service.FileService;
import com.cyl.springboot.service.RepairService;
import com.cyl.springboot.service.ServiceFileService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class RepairServiceImpl extends ServiceImpl<RepairMapper, Repair> implements RepairService {

    @Resource
    private RepairMapper repairMapper;

    @Resource
    private ServiceFileService serviceFileService;

    @Resource
    private FileService fileService;

    @Override
    public Page<RepairResponse> queryPage(RepairRequest request) {
        Page page = new Page(request.getPageNo(), request.getPageSize());
        Page result = repairMapper.queryPage(page, request);
        List<RepairResponse> records = result.getRecords();
        if (CollectionUtils.isNotEmpty(records)) {
            for (RepairResponse record : records) {
                LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper.eq(ServiceFile::getServiceId, record.getRepairId());
                List<ServiceFile> list = serviceFileService.list(lambdaQueryWrapper);
                if (CollectionUtils.isNotEmpty(list)) {
                    LambdaQueryWrapper<Files> queryWrapper = new LambdaQueryWrapper<>();
                    List<String> fileIdList = new ArrayList<>();
                    for (ServiceFile serviceFile : list) {
                        fileIdList.add(serviceFile.getFileId());
                    }
                    queryWrapper.in(Files::getFileId, fileIdList);
                    List<Files> filesList = fileService.list(queryWrapper);
                    if (CollectionUtils.isNotEmpty(filesList)) {
                        List<String> urlList = new ArrayList<>();
                        for (Files files : filesList) {
                            urlList.add(files.getUrl());
                        }
                        record.setUrlList(urlList);
                        record.setFilesList(filesList);
                    }
                }
            }
        }
        result.setRecords(records);
        return result;
    }

    @Override
    public boolean save(RepairSaveRequest request) {
        Repair repair = new Repair();
        String repairId = IdUtil.fastSimpleUUID();
        BeanUtil.copyProperties(request, repair, false);
        repair.setRepairId(repairId);
        this.save(repair);
        saveServiceFile(request, repairId);
        return true;
    }

    @Override
    public boolean update(RepairSaveRequest request) {
        Repair repair = new Repair();
        BeanUtil.copyProperties(request, repair, false);
        this.updateById(repair);
        LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ServiceFile::getServiceId, request.getRepairId());
        serviceFileService.remove(lambdaQueryWrapper);
        saveServiceFile(request, request.getRepairId());
        return true;
    }

    private void saveServiceFile(RepairSaveRequest request, String repairId) {
        if (CollectionUtils.isNotEmpty(request.getFileIdList())) {
            List<ServiceFile> list = new ArrayList<>();
            for (String fileId : request.getFileIdList()) {
                ServiceFile serviceFile = new ServiceFile();
                serviceFile.setFileId(fileId);
                serviceFile.setServiceId(repairId);
                list.add(serviceFile);
            }
            serviceFileService.saveBatch(list);
        }
    }

}
