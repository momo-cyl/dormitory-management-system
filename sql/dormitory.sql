/*
 Navicat Premium Data Transfer

 Source Server         : 本机数据库
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : dormitory

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 08/03/2023 19:52:10
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT,
    `name`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
    `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
    `user`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '发布人',
    `time`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '发布时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article`
VALUES (1, '的文章标题',
        '# 我是\n## 我是2号\n\n::: hljs-center\n\n***++~~==我是那个哥哥==~~++***\n\n:::\n\n> 我是的引用\n\n我是B站：程序员青戈\n\n[百度](https://www.baidu.com)\n\n```java\nclass Hello {\n  public static void main(String[] args) {\n    System.out.pringln(\"Hello \");\n  }\n}\n\n```\n\n![搜狗截图20220129174103.png](http://localhost:9090/file/8567a00d2bf740e0a63794baf600cec3.png)\n\n\n',
        '程序员青戈', '2022-03-22 19:22:58');
INSERT INTO `article`
VALUES (2, '文章2号',
        '文章2号\n\n文章2号\n\n文章2号\n\n![QQ图片20220307194920.png](http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png)',
        '程序员青戈', '2022-03-22 19:22:58');

-- ----------------------------
-- Table structure for dormitory
-- ----------------------------
DROP TABLE IF EXISTS `dormitory`;
CREATE TABLE `dormitory`
(
    `id`                 int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dormitory_id`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '宿舍id',
    `dormitory_number`   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '宿舍编号',
    `dormitory_building` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '宿舍楼栋',
    `sanitation`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '卫生情况',
    `grade`              int(11) NULL DEFAULT NULL COMMENT '评分',
    `create_time`        timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `update_time`        timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dormitory
-- ----------------------------
INSERT INTO `dormitory`
VALUES (11, 'd3d42f655b8f427eafcdf29b3bbede4e', '南119', '0号楼', '太差劲了', 1, '2023-02-23 17:06:21', '2023-02-23 17:52:39');
INSERT INTO `dormitory`
VALUES (12, 'fe3eae8b89e44c9ebbde90def5bc67d0', '333', '123444', '可以', 4, '2023-02-23 17:50:09', '2023-03-03 23:21:40');
INSERT INTO `dormitory`
VALUES (13, 'ba1ceaf5940341ce935c9d9f3990e552', '1', '1', '', NULL, '2023-03-04 20:45:56', NULL);
INSERT INTO `dormitory`
VALUES (14, '9764eae6ec6b42bba4d4d17224af34e0', '2', '2', NULL, NULL, '2023-03-04 20:46:00', NULL);
INSERT INTO `dormitory`
VALUES (15, '3a3d5f46f353465aaa1b0a6b13a4b9c9', '3', '3', NULL, NULL, '2023-03-04 20:46:02', NULL);
INSERT INTO `dormitory`
VALUES (16, '64980b3fdd3b43008341d1e743929821', '4', '4', NULL, NULL, '2023-03-04 20:46:06', NULL);
INSERT INTO `dormitory`
VALUES (17, '545236912b334297b0c6ef3893551aa3', '5', '5', NULL, NULL, '2023-03-04 20:46:09', NULL);
INSERT INTO `dormitory`
VALUES (18, '6b58cfa1b29f446d8cf3f5bfbe89d266', '6', '6', NULL, NULL, '2023-03-04 20:46:13', NULL);
INSERT INTO `dormitory`
VALUES (19, 'c634d3b8d4ec469c82313c9cafa96495', '7', '7', NULL, NULL, '2023-03-04 20:46:17', NULL);
INSERT INTO `dormitory`
VALUES (20, 'b839ea8eb14b43c9acfc216c7dd82eca', '8', '8', NULL, NULL, '2023-03-04 20:46:21', NULL);
INSERT INTO `dormitory`
VALUES (21, '05fc1b245ab848dcb78461178439fbe4', '9', '9', NULL, NULL, '2023-03-04 20:46:24', NULL);
INSERT INTO `dormitory`
VALUES (22, 'f2fd4e39d9c04258b0b86eff90314481', '999', '999', '123', NULL, '2023-03-04 22:23:49', NULL);
INSERT INTO `dormitory`
VALUES (23, 'c8d449d94e22470eac03f4759f56722f', '9990', '9990', '000', NULL, '2023-03-04 22:34:14', NULL);
INSERT INTO `dormitory`
VALUES (24, '45e90405422847578dbaff8c74f78b2a', '123123', '3123123123', NULL, NULL, '2023-03-04 22:34:44', NULL);
INSERT INTO `dormitory`
VALUES (25, '012fa70f682f4c4da365114cc2c1500f', '123', '231', NULL, NULL, '2023-03-04 22:36:04', '2023-03-05 14:48:10');
INSERT INTO `dormitory`
VALUES (26, '37b73c3e5ce04a07852b0fbbc6a1178a', '1', '宿舍1', '还行吧', NULL, '2023-03-05 19:56:26', NULL);

-- ----------------------------
-- Table structure for dormitory_user
-- ----------------------------
DROP TABLE IF EXISTS `dormitory_user`;
CREATE TABLE `dormitory_user`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dormitory_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '宿舍id',
    `user_id`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户id',
    `create_time`  timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `update_time`  timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 150 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dormitory_user
-- ----------------------------
INSERT INTO `dormitory_user`
VALUES (132, 'd3d42f655b8f427eafcdf29b3bbede4e', '1', '2023-02-23 17:52:39', NULL);
INSERT INTO `dormitory_user`
VALUES (146, 'fe3eae8b89e44c9ebbde90def5bc67d0', '16', '2023-03-04 21:43:17', NULL);
INSERT INTO `dormitory_user`
VALUES (147, 'fe3eae8b89e44c9ebbde90def5bc67d0', '17', '2023-03-04 21:43:17', NULL);
INSERT INTO `dormitory_user`
VALUES (148, 'f2fd4e39d9c04258b0b86eff90314481', '25', '2023-03-04 22:23:49', NULL);
INSERT INTO `dormitory_user`
VALUES (149, '37b73c3e5ce04a07852b0fbbc6a1178a', '28', '2023-03-05 19:56:26', NULL);

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `user_id`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '用户id',
    `address`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '签到地址',
    `latitude`     decimal(20, 6)                                         NOT NULL COMMENT '精度',
    `longitude`    decimal(20, 6)                                         NOT NULL COMMENT '纬度',
    `sign_in_time` timestamp(0) NULL DEFAULT NULL COMMENT '签到时间',
    `create_time`  timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `update_time`  timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of location
-- ----------------------------
INSERT INTO `location`
VALUES (20, '1', '江苏省南京市玄武区北京东路41号', 32.058380, 118.796470, '2023-03-05 11:58:19', '2023-03-05 19:58:19', NULL);

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `repair_id`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '业务主键id',
    `user_id`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '用户id',
    `dormitory_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '宿舍id',
    `status`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '状态',
    `description`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `create_time`  timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `update_time`  timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '站点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair`
VALUES (18, '330c8dc0391941128ea7419f7d4fe9a2', '17', 'fe3eae8b89e44c9ebbde90def5bc67d0', '已处理', '321312123',
        '2023-02-23 16:15:22', '2023-03-05 20:19:44');
INSERT INTO `repair`
VALUES (19, 'e10f3bfda8564e9db280dcb58ee2c828', '1', 'd3d42f655b8f427eafcdf29b3bbede4e', '已处理', '水管坏了',
        '2023-03-05 00:19:10', '2023-03-05 10:04:56');

-- ----------------------------
-- Table structure for service_file
-- ----------------------------
DROP TABLE IF EXISTS `service_file`;
CREATE TABLE `service_file`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `file_id`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件id',
    `service_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '业务id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`
(
    `name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
    `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容',
    `type`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict`
VALUES ('user', 'el-icon-user', 'icon');
INSERT INTO `sys_dict`
VALUES ('house', 'el-icon-house', 'icon');
INSERT INTO `sys_dict`
VALUES ('menu', 'el-icon-menu', 'icon');
INSERT INTO `sys_dict`
VALUES ('s-custom', 'el-icon-s-custom', 'icon');
INSERT INTO `sys_dict`
VALUES ('s-grid', 'el-icon-s-grid', 'icon');
INSERT INTO `sys_dict`
VALUES ('document', 'el-icon-document', 'icon');
INSERT INTO `sys_dict`
VALUES ('coffee', 'el-icon-coffee\r\n', 'icon');
INSERT INTO `sys_dict`
VALUES ('s-marketing', 'el-icon-s-marketing', 'icon');
INSERT INTO `sys_dict`
VALUES ('aidKit', 'el-icon-first-aid-kit', 'icon');
INSERT INTO `sys_dict`
VALUES ('dormitoryManagement', 'el-icon-office-building', 'icon');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`
(
    `id`        int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `file_id`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件唯一id',
    `name`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件名称',
    `type`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件类型',
    `size`      bigint(20) NULL DEFAULT NULL COMMENT '文件大小(kb)',
    `url`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '下载链接',
    `md5`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件md5',
    `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除',
    `enable`    tinyint(1) NULL DEFAULT 1 COMMENT '是否禁用链接',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 198 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
    `path`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '路径',
    `icon`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图标',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
    `pid`         int(11) NULL DEFAULT NULL COMMENT '父级id',
    `page_path`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '页面路径',
    `sort_num`    int(11) NULL DEFAULT NULL COMMENT '排序',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu`
VALUES (4, '系统管理', NULL, 'el-icon-s-grid', NULL, NULL, NULL, 300);
INSERT INTO `sys_menu`
VALUES (5, '用户管理', '/user', 'el-icon-user', NULL, 4, 'User', 301);
INSERT INTO `sys_menu`
VALUES (6, '角色管理', '/role', 'el-icon-s-custom', NULL, 4, 'Role', 302);
INSERT INTO `sys_menu`
VALUES (7, '菜单管理', '/menu', 'el-icon-menu', NULL, 4, 'Menu', 303);
INSERT INTO `sys_menu`
VALUES (8, '文件管理', '/file', 'el-icon-document', NULL, 4, 'File', 304);
INSERT INTO `sys_menu`
VALUES (10, '主页', '/home', 'el-icon-house', NULL, NULL, 'Home', 0);
INSERT INTO `sys_menu`
VALUES (40, '高德地图', '/map', 'el-icon-house', NULL, NULL, 'Map', 999);
INSERT INTO `sys_menu`
VALUES (41, '文章管理', '/article', 'el-icon-menu', NULL, NULL, 'Article', 999);
INSERT INTO `sys_menu`
VALUES (44, '报修管理', '/aidKit', 'el-icon-first-aid-kit', NULL, NULL, 'AidKit', 261);
INSERT INTO `sys_menu`
VALUES (45, '宿舍管理', '/dormitory', 'el-icon-office-building', NULL, NULL, 'Dormitory', 200);
INSERT INTO `sys_menu`
VALUES (46, '到访管理', '/visit', 'el-icon-view', NULL, NULL, 'Visit', 201);
INSERT INTO `sys_menu`
VALUES (47, '签到管理', '/Location', 'el-icon-location-information', NULL, NULL, 'Location', 202);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
    `flag`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '唯一标识',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role`
VALUES (1, '管理员', '管理员', 'ROLE_ADMIN');
INSERT INTO `sys_role`
VALUES (3, '用户', '学生', 'ROLE_USER');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `role_id` int(11) NOT NULL COMMENT '角色id',
    `menu_id` int(11) NOT NULL COMMENT '菜单id',
    PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色菜单关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu`
VALUES (1, 4);
INSERT INTO `sys_role_menu`
VALUES (1, 5);
INSERT INTO `sys_role_menu`
VALUES (1, 6);
INSERT INTO `sys_role_menu`
VALUES (1, 7);
INSERT INTO `sys_role_menu`
VALUES (1, 8);
INSERT INTO `sys_role_menu`
VALUES (1, 10);
INSERT INTO `sys_role_menu`
VALUES (1, 40);
INSERT INTO `sys_role_menu`
VALUES (1, 41);
INSERT INTO `sys_role_menu`
VALUES (1, 44);
INSERT INTO `sys_role_menu`
VALUES (1, 45);
INSERT INTO `sys_role_menu`
VALUES (1, 46);
INSERT INTO `sys_role_menu`
VALUES (1, 47);
INSERT INTO `sys_role_menu`
VALUES (3, 10);
INSERT INTO `sys_role_menu`
VALUES (3, 42);
INSERT INTO `sys_role_menu`
VALUES (3, 43);
INSERT INTO `sys_role_menu`
VALUES (3, 44);
INSERT INTO `sys_role_menu`
VALUES (3, 45);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `username`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
    `password`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
    `nickname`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '昵称',
    `email`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
    `phone`       varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
    `address`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地址',
    `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `avatar_url`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
    `role`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user`
VALUES (1, 'admin', 'admin', '程序员青戈', 'admin@qq.com', '13988997788', '江苏徐州', '2022-01-22 21:10:27',
        'http://localhost:9090/file/d3ccfe298954415182e8188c84d54508.jpg', 'ROLE_ADMIN');
INSERT INTO `sys_user`
VALUES (16, '222', '123', '甄姬好大', '2', '2', '2', '2022-02-26 22:10:14', NULL, 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (17, '333', '123', '我是三三哦豁', '3', '3', '3', '2022-02-26 22:10:18',
        'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg', 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (18, 'nzz', '123', '哪吒', '2', '2', '2', '2022-03-29 16:59:44', '', 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (19, 'yss', '123', '亚瑟', '3', '3', '3', '2022-04-29 16:59:44', '', 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (20, 'lxx', '123', '李信', '2', '2', '2', '2022-05-29 17:12:04', '2', 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (25, 'sir', '123', '安琪拉', NULL, NULL, NULL, '2022-06-08 17:00:47', NULL, 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (26, 'err', NULL, '妲己', '11', '1', '1', '2022-07-08 17:20:01', NULL, 'ROLE_USER');
INSERT INTO `sys_user`
VALUES (28, 'ddd', '123', 'ddd', '', '', '', '2022-11-09 10:41:07',
        'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg', 'ROLE_USER');

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `content`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容',
    `user_id`    int(11) NULL DEFAULT NULL COMMENT '评论人id',
    `time`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论时间',
    `pid`        int(11) NULL DEFAULT NULL COMMENT '父id',
    `origin_id`  int(11) NULL DEFAULT NULL COMMENT '最上级评论id',
    `article_id` int(11) NULL DEFAULT NULL COMMENT '关联文章的id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment`
VALUES (1,
        '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试',
        1, '2022-03-22 20:00:00', NULL, NULL, 1);
INSERT INTO `t_comment`
VALUES (2, '123', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_comment`
VALUES (5, '回复内容', 1, '2022-03-22 21:01:00', NULL, NULL, 1);
INSERT INTO `t_comment`
VALUES (6, '4444', 1, '2022-03-22 21:03:15', 4, 4, 1);
INSERT INTO `t_comment`
VALUES (7, '5555', 1, '2022-03-22 21:04:11', 4, 4, 1);
INSERT INTO `t_comment`
VALUES (8, '444444', 1, '2022-03-22 21:29:55', 7, 7, 1);
INSERT INTO `t_comment`
VALUES (9, '5555', 1, '2022-03-22 21:30:04', 7, 7, 1);
INSERT INTO `t_comment`
VALUES (10, '666', 1, '2022-03-22 21:34:05', 7, 4, 1);
INSERT INTO `t_comment`
VALUES (11, '甄姬真的好大好大！！', 16, '2022-03-22 21:38:26', 10, 4, 1);
INSERT INTO `t_comment`
VALUES (13, '哈哈哈哈，我是ddd', 28, '2022-03-22 21:46:01', 12, 12, 1);
INSERT INTO `t_comment`
VALUES (14, '我是李信，我很萌', 20, '2022-03-22 21:46:48', 13, 12, 1);
INSERT INTO `t_comment`
VALUES (15, '我在回复ddd', 20, '2022-03-22 21:47:03', 13, 12, 1);
INSERT INTO `t_comment`
VALUES (16, '我是李信', 20, '2022-03-22 21:48:19', 4, 4, 1);
INSERT INTO `t_comment`
VALUES (17, '33333', 20, '2022-03-22 21:48:42', 5, 5, 1);
INSERT INTO `t_comment`
VALUES (19, '我是李信嗯嗯嗯', 20, '2022-03-22 21:49:21', 1, 1, 1);
INSERT INTO `t_comment`
VALUES (21, '哈哈哈 我是ddd', 28, '2022-03-22 21:50:04', 20, 1, 1);

-- ----------------------------
-- Table structure for visit
-- ----------------------------
DROP TABLE IF EXISTS `visit`;
CREATE TABLE `visit`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '姓名',
    `sex`         varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '性别',
    `age`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '年龄',
    `phone`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '手机号',
    `destination` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '目的地',
    `visit_date`  timestamp(0) NULL DEFAULT NULL COMMENT '到访日期',
    `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP (0) COMMENT '创建时间',
    `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of visit
-- ----------------------------
INSERT INTO `visit`
VALUES (15, '张三123', '女', '18', '1111', '1', '2020-03-21 00:00:00', '2023-02-23 00:00:00', '2023-03-05 11:19:37');
INSERT INTO `visit`
VALUES (16, '123', '男', '123', '123', '123', NULL, '2023-03-05 11:26:31', NULL);
INSERT INTO `visit`
VALUES (17, '123', '男', '123', '123', '123', '2020-06-01 00:00:00', '2023-03-05 11:26:46', NULL);

SET
FOREIGN_KEY_CHECKS = 1;
