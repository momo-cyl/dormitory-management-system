package com.cyl.springboot.request.notice;

import com.cyl.springboot.request.PageRequest;
import lombok.Data;

import java.util.List;


@Data
public class NoticeRequest extends PageRequest {

    //学院名称(模糊查询)
    private String titleLike;

    private List<Long> idList;
}
