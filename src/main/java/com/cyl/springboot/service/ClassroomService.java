package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Classroom;
import com.cyl.springboot.request.classroom.ClassroomRequest;
import com.cyl.springboot.request.classroom.ClassroomSaveRequest;
import com.cyl.springboot.request.classroom.ClassroomUpdateRequest;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface ClassroomService extends IService<Classroom> {

    Classroom getById(Long id);

    Page<Classroom> queryPage(ClassroomRequest classroomRequest);

    Boolean save(ClassroomSaveRequest classroomSaveRequest);

    Boolean update(ClassroomUpdateRequest classroomUpdateRequest);

    Boolean deleteByIdList(List<Long> idList);
}
