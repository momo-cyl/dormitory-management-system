package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Major;
import com.cyl.springboot.request.major.MajorRequest;
import org.apache.ibatis.annotations.Param;

public interface MajorMapper extends BaseMapper<Major> {


    Page<Major> queryPage(Page<Major> page, @Param("request") MajorRequest request);
}
