package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.DormitoryUser;

public interface DormitoryUserMapper extends BaseMapper<DormitoryUser> {


}
