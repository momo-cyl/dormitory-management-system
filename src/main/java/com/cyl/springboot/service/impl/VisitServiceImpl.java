package com.cyl.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Visit;
import com.cyl.springboot.mapper.VisitMapper;
import com.cyl.springboot.request.VisitRequest;
import com.cyl.springboot.response.VisitResponse;
import com.cyl.springboot.service.VisitService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VisitServiceImpl extends ServiceImpl<VisitMapper, Visit> implements VisitService {

    @Override
    public Page<VisitResponse> queryPage(VisitRequest request) {
        Page page = new Page(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<Visit> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getName())) {
            lambdaQueryWrapper.like(Visit::getName, request.getName());
        }
        if (StrUtil.isNotBlank(request.getPhone())) {
            lambdaQueryWrapper.like(Visit::getPhone, request.getPhone());
        }
        lambdaQueryWrapper.orderByDesc(Visit::getCreateTime);
        Page result = this.page(page, lambdaQueryWrapper);
        List<VisitResponse> list = new ArrayList<>();
        List<Visit> records = result.getRecords();
        if (CollectionUtils.isNotEmpty(records)) {
            for (Visit record : records) {
                VisitResponse response = new VisitResponse();
                BeanUtil.copyProperties(record, response, false);
                list.add(response);
            }
        }
        result.setRecords(list);
        return result;
    }

}
