package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Major;
import com.cyl.springboot.request.major.MajorRequest;
import com.cyl.springboot.request.major.MajorSaveRequest;
import com.cyl.springboot.request.major.MajorUpdateRequest;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface MajorService extends IService<Major> {

    Major getById(Long id);

    Page<Major> queryPage(MajorRequest majorRequest);

    Boolean save(MajorSaveRequest majorSaveRequest);

    Boolean update(MajorUpdateRequest majorUpdateRequest);

    Boolean deleteByIdList(List<Long> idList);
}
