package com.cyl.springboot.request;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class DormitorySaveRequest {

    //ID
    private Integer id;

    //宿舍id
    private String dormitoryId;

    //宿舍编号
    private String dormitoryNumber;

    //宿舍楼栋
    private String dormitoryBuilding;

    //图片链接
    private String avatarUrl;

    //用户ids
    private List<String> userIdList;

    //文件id列表
    private List<String> fileIdList;

    //卫生情况
    private String sanitation;

    //宿舍评分
    private Integer grade;

}
