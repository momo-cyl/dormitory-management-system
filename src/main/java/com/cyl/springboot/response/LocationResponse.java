package com.cyl.springboot.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
public class LocationResponse {

    private Integer id;

    private String userId;

    private String userName;

    private String address;

    private String phone;

    private BigDecimal latitude;

    private BigDecimal longitude;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signInTime;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
