package com.cyl.springboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Dormitory;
import com.cyl.springboot.request.DormitoryDeleteRequest;
import com.cyl.springboot.request.DormitoryRequest;
import com.cyl.springboot.request.DormitorySaveRequest;
import com.cyl.springboot.response.DormitoryResponse;

public interface DormitoryService extends IService<Dormitory> {

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    Page<DormitoryResponse> queryPage(DormitoryRequest request);

    /**
     * 保存
     *
     * @param dormitorySaveRequest
     * @return
     */
    boolean save(DormitorySaveRequest dormitorySaveRequest);

    /**
     * 保存
     *
     * @param dormitorySaveRequest
     * @return
     */
    boolean update(DormitorySaveRequest dormitorySaveRequest);

    /**
     * 删除
     *
     * @param deleteRequest
     * @return
     */
    boolean delete(DormitoryDeleteRequest deleteRequest);

    /**
     * 批量删除
     *
     * @param deleteRequest
     * @return
     */
    boolean batchDelete(DormitoryDeleteRequest deleteRequest);


}
