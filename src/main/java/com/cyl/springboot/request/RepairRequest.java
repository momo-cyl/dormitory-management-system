package com.cyl.springboot.request;

import lombok.Data;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class RepairRequest extends PageRequest {

    //宿舍楼栋
    private String dormitoryBuilding;

    //宿舍编号
    private String dormitoryNumber;

    //用户名
    private String userName;

    //手机号
    private String phone;

    //状态
    private String status;

    //用户id
    private String userId;

}
