package com.cyl.springboot.request.major;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class MajorUpdateRequest {

    //主键id
    private Long id;

    //学院名称
    private String name;

    @ApiModelProperty("学院id")
    private Long collegeId;
}
