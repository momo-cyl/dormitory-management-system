package com.cyl.springboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Visit;
import com.cyl.springboot.request.VisitRequest;
import com.cyl.springboot.response.VisitResponse;

public interface VisitService extends IService<Visit> {


    Page<VisitResponse> queryPage(VisitRequest request);

}
