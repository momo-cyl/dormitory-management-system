Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		info: {},
		userList: []
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		this.list();
	},
	list(){
		let that = this;
		wx.request({
			url: `http://114.116.117.208:9090/dormitoryUser/getDormitoryUserList`,
			method: 'POST',
			data: {
		
			},
			success: function(res) {
				let arr = [];
				let arr2 = [];
				that.data.info.urlList = arr;
				that.data.info.fileIdList = arr2;
				let info = that.data.info;
				console.log(info)
				that.setData({
					info: info,
					userList: res.data
				});
				console.log(that.data.userList);
			}
		})
	},
	checkboxChange(e) {
		let index = e.target.dataset.index;
		let arr = this.data.userList;
		arr[index].status = "USING";
		this.setData({
			userList: arr
		})
	},
	bindKeyInput: function(e) {
		this.data.info[`${e.currentTarget.dataset.params}`] = e.detail.value
		this.setData({
			info: this.data.info
		})
		console.log(`info对象：`, this.data.info)
	},
	deleteImage(e) {
		let index = e.currentTarget.dataset.params;
		let arr = this.data.info.urlList;
		let arr2 = this.data.info.fileIdList;
		arr.splice(index, 1);
		arr2.splice(index, 1);
		this.data.info.urlList = arr;
		this.data.info.fileIdList = arr2;
		console.log(this.data.info);
		this.setData({
			info: this.data.info
		})
	},
	formSubmit: function(e) {
		const that = this;
		let request = this.data.info;
		let idList = [];
		if(request.dormitoryBuilding == null || request.dormitoryBuilding == ''){
			wx.showToast({
				title: '请输入宿舍楼栋',
				icon: 'none'
			})
			return;
		}
		if(request.dormitoryNumber == null || request.dormitoryNumber == ''){
			wx.showToast({
				title: '请输入宿舍号',
				icon: 'none'
			})
			return;
		}
		if(this.data.userList != null){
			for (var i = 0; i < this.data.userList.length; i++) {
				if (this.data.userList[i].status == 'USING') {
					idList.push(this.data.userList[i].userId);
				}
			}
		}
		request.userIdList = idList;
		console.log(this.data);
		wx.request({
			url: `http://114.116.117.208:9090/dormitory/save`,
			method: 'POST',
			data: {
				id: request.id,
				dormitoryId: request.dormitoryId,
				dormitoryBuilding: request.dormitoryBuilding,
				dormitoryNumber: request.dormitoryNumber,
				fileIdList: request.fileIdList,
				sanitation: request.sanitation,
				userIdList: request.userIdList
			},
			success: function(res) {
				if (res.data.code === '200') {
					that.setData({
						info: {}
					});
					that.list();
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					});
				}
			}
		})
	},
	uploadPhoto(e) { // 拍摄或从相册选取上传
		let that = this;
		wx.chooseImage({
			count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success(res) {
				let tempFilePaths = res.tempFilePaths; // 返回选定照片的本地路径列表 
				that.upload(that, tempFilePaths);
			}
		})
	},
	upload(page, path) { // 上传图片
		let result = this.data.info;
		let that = this;
		wx.showToast({
			icon: "loading",
			title: "正在上传……"
		});
		wx.uploadFile({
			url: 'http://114.116.117.208:9090/file/saveFile', //后端接口
			filePath: path[0],
			name: 'file',
			header: {
				"Content-Type": "multipart/form-data"
			},
			success(res) {
				let str = res.data.replace(/\ufeff/g, "") // step1: 首先去掉两头的"",
				let data = JSON.parse(str) // step2: 转换成JSON格式的数据
				console.log(data);
				console.log(res);
				if (res.statusCode != 200) {
					wx.showModal({
						title: '提示',
						content: '上传失败',
						showCancel: false
					});
					return;
				} else {
					console.log(result);
					result.urlList.push(data.url);
					result.fileIdList.push(data.fileId);
					console.log(result);
					that.setData({
						info: result
					});
				}
			},
			fail(e) {
				wx.showModal({
					title: '提示',
					content: '上传失败',
					showCancel: false
				});
			},
			complete() {
				wx.hideToast(); //隐藏Toast
			}
		})
	},


	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
