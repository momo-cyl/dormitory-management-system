package com.cyl.springboot.request.classroom;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
public class ClassroomUpdateRequest {

    //主键id
    private Long id;

    @ApiModelProperty("班级名称")
    private String name;

    @ApiModelProperty("辅导员")
    private String counselor;

    @ApiModelProperty("学院id")
    private Long collegeId;

    @ApiModelProperty("专业id")
    private Long majorId;

    @ApiModelProperty("入学年份")
    private Date enrollDate;
}
