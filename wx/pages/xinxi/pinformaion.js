// pages/xinxi/pinformaion.js
var app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		userInfo: {}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
		const that = this;
		wx.request({
			url: `http://114.116.117.208:9090/user/username/` + edusysUserInfo.username,
			method: 'GET',
			success: function(res) {
				if (res.data.code === '200') {
					var result = res.data.data;
					console.log(result);
					that.setData({
						userInfo: result
					})
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					});
					wx.clearStorageSync()
					wx.setData({
						username: ''
					})
					wx.navigateTo({
						url: "login"
					})
				}
			}
		})
	},

	bindKeyInput: function(e) {
		this.data.userInfo[`${e.currentTarget.dataset.params}`] = e.detail.value
		this.setData({
			userInfo: this.data.userInfo
		})
		console.log(`userInfo对象：`, this.data.userInfo)
	},

	formSubmit: function(e) {
		const that = this;
		let request = this.data.userInfo;
		if (request.id == null) {
			return;
		}
		console.log(request)
		wx.request({
			url: `http://114.116.117.208:9090/user/saveOrUpdate`,
			method: 'POST',
			data: {
				id: request.id,
				nickname: request.nickname,
				email: request.email,
				phone: request.phone,
				avatarUrl: request.avatarUrl,
				address: request.address
			},
			success: function(res) {
				if (res.data.code === '200') {
					wx.setStorage({
						data: request,
						key: 'edusysUserInfo'
					})
					app.globalData.edusysUserInfo = request
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					});
				}
			}
		})
	},
	uploadPhoto(e) { // 拍摄或从相册选取上传
		let that = this;
		wx.chooseImage({
			count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success(res) {
				let tempFilePaths = res.tempFilePaths; // 返回选定照片的本地路径列表 
				that.upload(that, tempFilePaths);
			}
		})
	},
	upload(page, path) { // 上传图片
		let result = this.data.userInfo;
		let that = this;
		wx.showToast({
			icon: "loading",
			title: "正在上传……"
		});
		wx.uploadFile({
			url: 'http://114.116.117.208:9090/file/upload', //后端接口
			filePath: path[0],
			name: 'file',
			header: {
				"Content-Type": "multipart/form-data"
			},
			success(res) {
				console.log(res);
				if (res.statusCode != 200) {
					wx.showModal({
						title: '提示',
						content: '上传失败',
						showCancel: false
					});
					return;
				} else {
					result.avatarUrl = res.data;
					console.log(result);
					that.setData({
						userInfo: result
					})
					wx.setStorage({
						data: result,
						key: 'edusysUserInfo'
					})
					app.globalData.edusysUserInfo = result
				}
			},
			fail(e) {
				wx.showModal({
					title: '提示',
					content: '上传失败',
					showCancel: false
				});
			},
			complete() {
				wx.hideToast(); //隐藏Toast
			}
		})
	},


	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
