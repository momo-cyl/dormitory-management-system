package com.cyl.springboot.request;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class VisitDeleteRequest {

    //主键id
    private Integer id;

    //主键ids
    private List<Integer> ids;
}
