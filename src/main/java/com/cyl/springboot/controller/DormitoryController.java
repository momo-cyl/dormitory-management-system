package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.common.Result;
import com.cyl.springboot.entity.Dormitory;
import com.cyl.springboot.entity.ServiceFile;
import com.cyl.springboot.request.DormitoryDeleteRequest;
import com.cyl.springboot.request.DormitoryRequest;
import com.cyl.springboot.request.DormitorySaveRequest;
import com.cyl.springboot.response.DormitoryResponse;
import com.cyl.springboot.service.DormitoryService;
import com.cyl.springboot.service.ServiceFileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/dormitory")
public class DormitoryController {

    @Resource
    private DormitoryService dormitoryService;

    @Resource
    private ServiceFileService serviceFileService;

    /**
     * 根据ID查询站点信息
     *
     * @param id 主键
     * @return
     */
    @GetMapping("/get/{id}")
    public Dormitory getDormitory(@PathVariable("id") Integer id) {
        return dormitoryService.getById(id);
    }

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    @PostMapping("/queryPage")
    @ResponseBody
    public Page<DormitoryResponse> queryPage(@RequestBody DormitoryRequest request) {
        return dormitoryService.queryPage(request);
    }

    /**
     * 获取全部
     *
     * @param
     * @return
     */
    @PostMapping("/getAll")
    @ResponseBody
    public List<Dormitory> getAll() {
        return dormitoryService.list();
    }


    /**
     * 新增或更新
     *
     * @param dormitorySaveRequest
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody DormitorySaveRequest dormitorySaveRequest) {
        if (!dormitoryService.save(dormitorySaveRequest)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 更新
     *
     * @param dormitorySaveRequest
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody DormitorySaveRequest dormitorySaveRequest) {
        if (!dormitoryService.update(dormitorySaveRequest)) {
            return Result.error();
        }
        return Result.success();
    }


    /**
     * 删除
     *
     * @param dormitoryDeleteRequest
     * @return
     */
    @PostMapping("/delete")
    public Result deleteDormitory(@RequestBody DormitoryDeleteRequest dormitoryDeleteRequest) {
        if (!dormitoryService.delete(dormitoryDeleteRequest)) {
            return Result.error();
        }
        LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ServiceFile::getServiceId, dormitoryDeleteRequest.getDormitoryId());
        serviceFileService.remove(lambdaQueryWrapper);
        return Result.success();
    }

    /**
     * 批量删除
     *
     * @param dormitoryDeleteRequest 主键集合
     * @return
     */
    @PostMapping("/delete/batch")
    public Result deleteBatch(@RequestBody DormitoryDeleteRequest dormitoryDeleteRequest) {
        dormitoryService.batchDelete(dormitoryDeleteRequest);
        LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(ServiceFile::getServiceId, dormitoryDeleteRequest.getDormitoryIdList());
        serviceFileService.remove(lambdaQueryWrapper);
        return Result.success();
    }

}
