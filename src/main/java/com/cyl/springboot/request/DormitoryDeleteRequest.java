package com.cyl.springboot.request;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class DormitoryDeleteRequest {

    //宿舍id
    private String dormitoryId;

    //宿舍ids
    private List<String> dormitoryIdList;

}
