package com.cyl.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Files;

public interface FileMapper extends BaseMapper<Files> {
}
