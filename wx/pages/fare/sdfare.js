// pages/fare/sdfare.js
var app = getApp()
var domain = app.globalData.domain;
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		builId: '',
		wconsum: '0',
		econsum: '0',
		wcharge: '0',
		echarge: '0'

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		var that = this;
		//获取当前地理位置
		wx.chooseLocation({
			success: function(res) {
				var name = res.name
				var address = res.address
				var latitude = res.latitude
				var longitude = res.longitude
				console.log(address)
				console.log(name)
				console.log(latitude)
				console.log(longitude)
			},
			complete(r) {
				console.log(r)
				console.log(222)
			}
		})
		// wx.getLocation({
		//   type: 'wgs84', //默认为 wgs84 返回 gps 坐标，gcj02 返回可用于wx.openLocation的坐标
		//   success: (res) => {
		//     console.log(res)
		//     that.setData({
		//       latitude:res.latitude,  //纬度
		//       longitude:res.longitude,  //经度
		//     })
		//   },
		//   fail: (err) => {
		//     console.log(err)
		//   }
		// })

	},
	check: function() {
		var _this = this;
		_this.setData({
			remind: '加载中'
		});
		const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
		wx.request({
			url: 'http://localhost:8080/dor/servlet/FareServlet',
			data: {
				dorid: edusysUserInfo.dor_id,
				type: "0",
				//userForm:"wechat",
				// openid:app.globalData.openid,
			},
			timeout: app.globalData.requestTimeout,
			header: {
				'content-type': 'application/json' // 默认值
			},
			method: 'GET',
			success: function(res) {
				// console.log('eduSysProfile：', res.data)
				try {
					if (res.data.state == "true") {
						//res.data.password = password
						wx.setStorage({
							data: res.data,
							key: 'wadinformation'
						})
						app.globalData.edusysUserInfo = res.data
						wx.vibrateShort({
							type: 'medium'
						})

						//wx.switchTab({ url: './index' })
					}
				} catch (error) {
					// _this.getCookie();
					wx.showToast({
						title: res.data.message,
						icon: 'none',
						duration: 5000
					})
					_this.setData({
						remind: ''
					});
				}
			}
		})

	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {


	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
