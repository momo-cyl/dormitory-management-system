package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Classroom;
import com.cyl.springboot.request.classroom.ClassroomRequest;
import com.cyl.springboot.request.classroom.ClassroomSaveRequest;
import com.cyl.springboot.request.classroom.ClassroomUpdateRequest;
import com.cyl.springboot.service.ClassroomService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/classroomManage")
public class ClassroomManageController {

    @Resource
    private ClassroomService classroomService;

    @GetMapping("/get/{id}")
    public Classroom getById(@PathVariable("id") Long id) {
        return classroomService.getById(id);
    }

    @PostMapping("/queryPage")
    @ResponseBody
    public Page<Classroom> queryPage(@RequestBody ClassroomRequest classroomRequest) {
        return classroomService.queryPage(classroomRequest);
    }

    @PostMapping("/save")
    public Boolean save(@RequestBody ClassroomSaveRequest classroomSaveRequest) {
        return classroomService.save(classroomSaveRequest);
    }

    @PostMapping("/update")
    public Boolean update(@RequestBody ClassroomUpdateRequest classroomUpdateRequest) {
        return classroomService.update(classroomUpdateRequest);
    }

    @PostMapping("/delete")
    public Boolean delete(@RequestBody ClassroomRequest classroomRequest) {
        return classroomService.deleteByIdList(classroomRequest.getIdList());
    }
}
