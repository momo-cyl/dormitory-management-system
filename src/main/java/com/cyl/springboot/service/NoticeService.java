package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Notice;
import com.cyl.springboot.request.notice.NoticeRequest;
import com.cyl.springboot.request.notice.NoticeSaveRequest;
import com.cyl.springboot.request.notice.NoticeUpdateRequest;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface NoticeService extends IService<Notice> {
    Notice getById(Long id);

    Page<Notice> queryPage(NoticeRequest noticeRequest);

    Boolean save(NoticeSaveRequest noticeSaveRequest);

    Boolean update(NoticeUpdateRequest noticeUpdateRequest);

    Boolean deleteByIdList(List<Long> idList);
}
