package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
@TableName("major")
@ApiModel(value = "专业", description = "")
public class Major {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("专业名称")
    private String name;

    @ApiModelProperty("学院id")
    private Long collegeId;

    @ApiModelProperty("学院名称")
    private String collegeName;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("删除标记,0:未删除,1:已删除")
    private Boolean delFlag;
}
