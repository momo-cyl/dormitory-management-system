package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Visit;

public interface VisitMapper extends BaseMapper<Visit> {


}
