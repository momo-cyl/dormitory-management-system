package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.College;
import com.cyl.springboot.request.college.CollegeRequest;
import com.cyl.springboot.request.college.CollegeSaveRequest;
import com.cyl.springboot.request.college.CollegeUpdateRequest;
import com.cyl.springboot.service.CollegeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/collegeManage")
public class CollegeManageController {

    @Resource
    private CollegeService collegeService;

    @GetMapping("/get/{id}")
    public College getById(@PathVariable("id") Long id) {
        return collegeService.getById(id);
    }

    @PostMapping("/queryPage")
    @ResponseBody
    public Page<College> queryPage(@RequestBody CollegeRequest request) {
        return collegeService.queryPage(request);
    }

    @PostMapping("/save")
    public Boolean save(@RequestBody CollegeSaveRequest collegeSaveRequest) {
        return collegeService.save(collegeSaveRequest);
    }

    @PostMapping("/update")
    public Boolean update(@RequestBody CollegeUpdateRequest collegeUpdateRequest) {
        return collegeService.update(collegeUpdateRequest);
    }

    @PostMapping("/delete")
    public Boolean delete(@RequestBody CollegeRequest collegeRequest) {
        return collegeService.deleteByIdList(collegeRequest.getIdList());
    }
}
