package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Dormitory;

public interface DormitoryMapper extends BaseMapper<Dormitory> {
}
