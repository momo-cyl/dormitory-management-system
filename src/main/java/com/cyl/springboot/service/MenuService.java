package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Menu;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-02-10
 */
public interface MenuService extends IService<Menu> {

    List<Menu> findMenus(String name);
}
