package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.common.Result;
import com.cyl.springboot.entity.Visit;
import com.cyl.springboot.request.VisitDeleteRequest;
import com.cyl.springboot.request.VisitRequest;
import com.cyl.springboot.response.VisitResponse;
import com.cyl.springboot.service.VisitService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Objects;


@RestController
@RequestMapping("/visit")
public class VisitController {

    @Resource
    private VisitService visitService;


    /**
     * @param id 主键
     * @return
     */
    @GetMapping("/get/{id}")
    public Visit getVisit(@PathVariable("id") Integer id) {
        return visitService.getById(id);
    }

    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    @PostMapping("/queryPage")
    public Page<VisitResponse> queryPage(@RequestBody VisitRequest request) {
        return visitService.queryPage(request);
    }


    /**
     * 新增
     *
     * @param visit
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody Visit visit) {
        if (Objects.nonNull(visit.getVisitDate())) {
            LocalDateTime localDateTime = visit.getVisitDate().plusHours(8);
            visit.setVisitDate(localDateTime);
        }
        if (!visitService.save(visit)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 更新
     *
     * @param visit
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody Visit visit) {
        if (Objects.nonNull(visit.getVisitDate())) {
            LocalDateTime localDateTime = visit.getVisitDate().plusHours(8);
            visit.setVisitDate(localDateTime);
        }
        if (!visitService.updateById(visit)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键ID删除
     *
     * @param request 主键ID
     * @return
     */
    @PostMapping("/delete")
    public Result deleteRepair(@RequestBody VisitDeleteRequest request) {
        if (!visitService.removeById(request.getId())) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 根据主键集合批量删除
     *
     * @param request 主键集合
     * @return
     */
    @PostMapping("/delete/batch")
    public Result deleteBatch(@RequestBody VisitDeleteRequest request) {
        if (!visitService.removeByIds(request.getIds())) {
            return Result.error();
        }
        return Result.success();
    }

}
