package com.cyl.springboot.request.major;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class MajorSaveRequest {

    @ApiModelProperty("专业名称")
    private String name;

    @ApiModelProperty("学院id")
    private Long collegeId;

}
