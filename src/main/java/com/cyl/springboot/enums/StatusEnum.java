package com.cyl.springboot.enums;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
public enum StatusEnum {


    PROCESSED("PROCESSED", "已处理"),
    CANCEL("CANCEL", "作废"),
    UN_PROCESSED("UN_PROCESSED", "未处理");

    private String code;

    private String description;

    StatusEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
