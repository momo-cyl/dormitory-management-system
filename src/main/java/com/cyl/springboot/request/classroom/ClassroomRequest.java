package com.cyl.springboot.request.classroom;

import com.cyl.springboot.request.PageRequest;
import lombok.Data;

import java.util.List;


@Data
public class ClassroomRequest extends PageRequest {

    //学院名称(模糊查询)
    private String classroomNameLike;

    private String collegeNameLike;

    private String majorNameLike;

    private List<Long> idList;

    private Boolean delFlag;
}
