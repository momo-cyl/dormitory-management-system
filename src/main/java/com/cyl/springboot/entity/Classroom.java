package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
@TableName("classroom")
@ApiModel(value = "班级", description = "")
public class Classroom {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("班级名称")
    private String name;

    @ApiModelProperty("辅导员")
    private String counselor;

    @ApiModelProperty("学院id")
    private Long collegeId;

    @ApiModelProperty("专业id")
    private Long majorId;

    @ApiModelProperty("入学年份")
    private Date enrollDate;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("删除标记,0:未删除,1:已删除")
    private Boolean delFlag;
}
