package com.cyl.springboot.request;

import lombok.Data;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class DormitoryRequest extends PageRequest {

    //宿舍楼栋
    private String dormitoryBuilding;

    //宿舍编号
    private String dormitoryNumber;


}
