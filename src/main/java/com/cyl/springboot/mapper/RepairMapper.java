package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Repair;
import com.cyl.springboot.request.RepairRequest;
import com.cyl.springboot.response.RepairResponse;
import org.apache.ibatis.annotations.Param;

public interface RepairMapper extends BaseMapper<Repair> {

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    Page<RepairResponse> queryPage(Page<RepairRequest> page, @Param("request") RepairRequest request);


}
