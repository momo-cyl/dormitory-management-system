package com.cyl.springboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Classroom;
import com.cyl.springboot.mapper.ClassroomMapper;
import com.cyl.springboot.request.classroom.ClassroomRequest;
import com.cyl.springboot.request.classroom.ClassroomSaveRequest;
import com.cyl.springboot.request.classroom.ClassroomUpdateRequest;
import com.cyl.springboot.service.ClassroomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
@Service
public class ClassroomServiceImpl extends ServiceImpl<ClassroomMapper, Classroom> implements ClassroomService {

    @Resource
    private ClassroomMapper classroomMapper;

    @Override
    public Classroom getById(Long id) {
        LambdaQueryWrapper<Classroom> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Classroom::getId, id);
        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<Classroom> queryPage(ClassroomRequest classroomRequest) {
        Page<Classroom> page = new Page<>(classroomRequest.getPageNo(), classroomRequest.getPageSize());
        return classroomMapper.queryPage(page, classroomRequest);
    }

    @Override
    public Boolean save(ClassroomSaveRequest classroomSaveRequest) {
        Classroom classroom = new Classroom();
        classroom.setName(classroomSaveRequest.getName());
        classroom.setCollegeId(classroomSaveRequest.getCollegeId());
        classroom.setMajorId(classroomSaveRequest.getMajorId());
        classroom.setCounselor(classroomSaveRequest.getCounselor());
        classroom.setEnrollDate(classroomSaveRequest.getEnrollDate());
        return this.save(classroom);
    }

    @Override
    public Boolean update(ClassroomUpdateRequest classroomUpdateRequest) {
        LambdaQueryWrapper<Classroom> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Classroom::getId, classroomUpdateRequest.getId());
        Classroom classroom = new Classroom();
        classroom.setName(classroomUpdateRequest.getName());
        classroom.setCollegeId(classroomUpdateRequest.getCollegeId());
        classroom.setMajorId(classroomUpdateRequest.getMajorId());
        classroom.setCounselor(classroomUpdateRequest.getCounselor());
        classroom.setEnrollDate(classroomUpdateRequest.getEnrollDate());
        return this.update(classroom, lambdaQueryWrapper);
    }

    @Override
    public Boolean deleteByIdList(List<Long> idList) {
        LambdaQueryWrapper<Classroom> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(Classroom::getId, idList);
        Classroom classroom = new Classroom();
        classroom.setDelFlag(true);
        return this.update(classroom, lambdaQueryWrapper);
    }


}
