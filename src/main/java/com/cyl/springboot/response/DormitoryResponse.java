package com.cyl.springboot.response;

import com.cyl.springboot.entity.Files;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;


@Data
public class DormitoryResponse {

    //ID
    private Integer id;

    //宿舍id
    private String dormitoryId;

    //宿舍编号
    private String dormitoryNumber;

    //存储图片
    private List<Files> filesList;

    //图片url
    private List<String> urlList;

    //宿舍楼栋
    private String dormitoryBuilding;

    //卫生情况
    private String sanitation;

    //宿舍评分
    private Integer grade;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
