Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		title: '加载中...', // 状态
		list: [], // 数据列表
		loading: true, // 显示等待框
		pageNo: 1, // 初始页码
		pageSize: 10, // 每页十条数据，可根据实际修改
		pages: 0,
		oldList:[]
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
		if (edusysUserInfo.role == null) {
			wx.navigateTo({
				url: "../index/login"
			});
		} else {
			if (edusysUserInfo.role == "ROLE_ADMIN") { 
				this.page();
			} else {
				setTimeout(function() {
					wx.showToast({
						title: '没有权限',
						icon: 'none'
					})
				}, 10);
				wx.switchTab({
					url: "../page1/page1"
				})
			}
		}
	},
	page: function() {
		const that = this;
		wx.request({
			url: `http://114.116.117.208:9090/visit/queryPage`,
			method: 'POST',
			data: {
				pageNo: this.data.pageNo,
				pageSize: this.data.pageSize
			},
			success: function(res) {
				let result = res.data.records;
				let arr = that.data.oldList;
				console.log(res)
				if (result.length === 0) {
					wx.showToast({
						title: '没有更多数据了',
						icon: 'none',
						duration: 2000
					})
					return;
				}
				for (var i = 0; i < result.length; i++) {
					let fileIdList = [];
					if (result[i].filesList != null && result[i].filesList.length != 0) {
						for (var j = 0; j < result[i].filesList.length; j++) {
							fileIdList.push(result[i].filesList[j].fileId);
						}
						result[i].fileIdList = fileIdList;
					}
				}
				// 赋值
				that.setData({
					list: arr.concat(result),
					oldList: arr.concat(result),
					pages: res.data.pages,
					loading: false // 关闭等待框
				})
				console.log(that.data.list)
			}
		})
	},
	/** 页面上拉触底事件的处理函数 */
	onReachBottom() {
		let pageNo = ++this.data.pageNo
		// 数据是否全部加载完毕
		if (pageNo<=this.data.pages) {
			this.setData({
				pageNo: pageNo
			})
			setTimeout(() => {
				this.page() // 加载新数据
			}, 500)
		}
	}
})
