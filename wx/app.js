// app.js
var QQMapWX = require('/utils/qqmap-wx-jssdk.js')
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null,
	// 实例化API核心类
	qqmapsdk: new QQMapWX({
	  key: '7PRBZ-7XICX-A4J4M-ZXI7A-HM6RT-UIBFB' // 必填
	}),
  }
})
