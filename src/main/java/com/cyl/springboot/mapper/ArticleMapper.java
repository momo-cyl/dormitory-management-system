package com.cyl.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Article;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
