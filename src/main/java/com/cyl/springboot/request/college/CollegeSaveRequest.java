package com.cyl.springboot.request.college;

import lombok.Data;


@Data
public class CollegeSaveRequest{

    //学院名称
    private String name;

}
