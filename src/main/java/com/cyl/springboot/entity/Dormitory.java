package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("dormitory")
public class Dormitory {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    //宿舍id
    private String dormitoryId;

    //宿舍编号
    private String dormitoryNumber;

    //宿舍楼栋
    private String dormitoryBuilding;

    //卫生情况
    private String sanitation;

    //宿舍评分
    private Integer grade;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
