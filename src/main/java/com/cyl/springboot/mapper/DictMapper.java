package com.cyl.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Dict;

public interface DictMapper extends BaseMapper<Dict> {
}
