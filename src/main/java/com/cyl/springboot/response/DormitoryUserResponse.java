package com.cyl.springboot.response;

import lombok.Data;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
public class DormitoryUserResponse {

    //用户id
    private String userId;

    //用户名
    private String userName;

    //入住状态
    private String status;

    //手机号
    private String phone;

    //地址
    private String address;

}
