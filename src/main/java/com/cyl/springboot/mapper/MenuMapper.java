package com.cyl.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Menu;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author cyl
 * @since 2022-02-10
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
