package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * 维修表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("repair")
public class Repair {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    //业务主键id
    private String repairId;

    //用户id
    private String userId;

    //宿舍id
    private String dormitoryId;

    //状态
    private String status;

    //描述
    private String description;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
