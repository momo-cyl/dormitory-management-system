package com.cyl.springboot.request;

import lombok.Data;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class LocationRequest extends PageRequest {

    //用户名
    private String userName;

    //手机号
    private String phone;


}
