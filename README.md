## 技术栈
SpringBoot2 + Vue2 + ElementUI + Axios + Hutool + Mysql

## 配置相关：
#### 1. jdk version:1.8
#### 2. mysql version:5.7及以上
#### 3. nodeJs version:16及以上
#### 4.vue version:2

## 配置文件相关：
#### 1.需要将java代码中的数据库url、redis、文件上传的配置
#### 2.微信小程序中腾讯地图的key、请求url
#### 3.前端请求的url
#### 4.均替换为自己的

## 如果喜欢请给个Star！
## 如有部署需求可联系 wx:15152111812