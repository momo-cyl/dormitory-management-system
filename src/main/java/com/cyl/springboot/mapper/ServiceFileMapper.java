package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.ServiceFile;

public interface ServiceFileMapper extends BaseMapper<ServiceFile> {


}
