package com.cyl.springboot.request.college;

import lombok.Data;


@Data
public class CollegeUpdateRequest {

    //主键id
    private Long id;

    //学院名称
    private String name;

}
