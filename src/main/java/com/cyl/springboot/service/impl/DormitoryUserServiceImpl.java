package com.cyl.springboot.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.DormitoryUser;
import com.cyl.springboot.entity.User;
import com.cyl.springboot.mapper.DormitoryUserMapper;
import com.cyl.springboot.request.DormitoryUserRequest;
import com.cyl.springboot.response.DormitoryUserResponse;
import com.cyl.springboot.service.DormitoryUserService;
import com.cyl.springboot.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DormitoryUserServiceImpl extends ServiceImpl<DormitoryUserMapper, DormitoryUser> implements DormitoryUserService {

    @Resource
    private UserService userService;


    @Override
    public Page<DormitoryUser> queryPage(DormitoryUserRequest request) {
        Page page = new Page(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<DormitoryUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getDormitoryId())) {
            lambdaQueryWrapper.eq(DormitoryUser::getDormitoryId, request.getDormitoryId());
        }
        if (StrUtil.isNotBlank(request.getUserId())) {
            lambdaQueryWrapper.eq(DormitoryUser::getUserId, request.getUserId());
        }
        lambdaQueryWrapper.orderByDesc(DormitoryUser::getCreateTime);
        Page pages = this.page(page, lambdaQueryWrapper);
        return pages;
    }

    @Override
    public List<DormitoryUser> getList(DormitoryUserRequest request) {
        LambdaQueryWrapper<DormitoryUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getDormitoryId())) {
            lambdaQueryWrapper.eq(DormitoryUser::getDormitoryId, request.getDormitoryId());
        }
        if (StrUtil.isNotBlank(request.getUserId())) {
            lambdaQueryWrapper.eq(DormitoryUser::getUserId, request.getUserId());
        }
        if (CollectionUtils.isNotEmpty(request.getUserIdList())) {
            lambdaQueryWrapper.in(DormitoryUser::getUserId, request.getUserIdList());
        }
        lambdaQueryWrapper.orderByDesc(DormitoryUser::getCreateTime);
        return this.list(lambdaQueryWrapper);
    }

    @Override
    public boolean delete(DormitoryUserRequest request) {
        List<DormitoryUser> list = getList(request);
        if (CollectionUtils.isNotEmpty(list)) {
            List<Integer> ids = new ArrayList<>();
            for (DormitoryUser dormitoryUser : list) {
                ids.add(dormitoryUser.getId());
            }
            return this.removeByIds(ids);
        }
        return false;
    }

    @Override
    public List<DormitoryUserResponse> getDormitoryUserList(DormitoryUserRequest request) {
        List<DormitoryUserResponse> result = new ArrayList<>();
        List<DormitoryUser> list = new ArrayList<>();
        if (StringUtils.isNotBlank(request.getDormitoryId())) {
            list = getList(request);
        }
        Map<String, DormitoryUser> dormitoryUserMap = this.list().stream().collect(Collectors.toMap(DormitoryUser::getUserId, dormitoryUser -> dormitoryUser));
        List<String> ids = new ArrayList<>();
        if (!dormitoryUserMap.isEmpty()) {
            if (CollectionUtils.isNotEmpty(list)) {
                for (DormitoryUser dormitoryUser : list) {
                    ids.add(dormitoryUser.getUserId());
                }
                if (CollectionUtils.isNotEmpty(ids)) {
                    Map<Integer, User> userMap = userService.getList(ids).stream().collect(Collectors.toMap(User::getId, user -> user));
                    if (!userMap.isEmpty()) {
                        List<User> existUserList = userService.list();
                        for (User user : existUserList) {
                            DormitoryUserResponse response = new DormitoryUserResponse();
                            response.setUserId(user.getId().toString());
                            response.setUserName(user.getNickname());
                            response.setPhone(user.getPhone());
                            response.setAddress(user.getAddress());
                            if (userMap.containsKey(user.getId())) {
                                response.setStatus("USING");
                            } else {
                                response.setStatus("FREE");
                            }
                            result.add(response);
                        }
                    }
                }
            } else {
                List<User> userList = userService.getList(ids);
                if (CollectionUtils.isNotEmpty(userList)) {
                    for (User user : userList) {
                        if (!dormitoryUserMap.containsKey(user.getId().toString())) {
                            DormitoryUserResponse response = new DormitoryUserResponse();
                            response.setUserId(user.getId().toString());
                            response.setUserName(user.getNickname());
                            response.setPhone(user.getPhone());
                            response.setAddress(user.getAddress());
                            response.setStatus("FREE");
                            result.add(response);
                        }
                    }
                }
            }
        } else {
            List<User> userList = userService.list();
            if (CollectionUtils.isNotEmpty(userList)) {
                for (User user : userList) {
                    DormitoryUserResponse response = new DormitoryUserResponse();
                    response.setUserId(user.getId().toString());
                    response.setUserName(user.getNickname());
                    response.setPhone(user.getPhone());
                    response.setAddress(user.getAddress());
                    response.setStatus("FREE");
                    result.add(response);
                }
            }
        }
        return result;
    }
}
