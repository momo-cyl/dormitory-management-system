package com.cyl.springboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Major;
import com.cyl.springboot.mapper.MajorMapper;
import com.cyl.springboot.request.major.MajorRequest;
import com.cyl.springboot.request.major.MajorSaveRequest;
import com.cyl.springboot.request.major.MajorUpdateRequest;
import com.cyl.springboot.service.MajorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
@Service
public class MajorServiceImpl extends ServiceImpl<MajorMapper, Major> implements MajorService {

    @Resource
    private MajorMapper majorMapper;

    @Override
    public Major getById(Long id) {
        LambdaQueryWrapper<Major> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Major::getId, id);
        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<Major> queryPage(MajorRequest majorRequest) {
        Page<Major> page = new Page<>(majorRequest.getPageNo(), majorRequest.getPageSize());
        return majorMapper.queryPage(page, majorRequest);
    }

    @Override
    public Boolean save(MajorSaveRequest majorSaveRequest) {
        Major major = new Major();
        major.setName(majorSaveRequest.getName());
        major.setCollegeId(majorSaveRequest.getCollegeId());
        return this.save(major);
    }

    @Override
    public Boolean update(MajorUpdateRequest majorUpdateRequest) {
        LambdaQueryWrapper<Major> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Major::getId, majorUpdateRequest.getId());
        Major major = new Major();
        major.setName(majorUpdateRequest.getName());
        major.setCollegeId(majorUpdateRequest.getCollegeId());
        return this.update(major, lambdaQueryWrapper);
    }

    @Override
    public Boolean deleteByIdList(List<Long> idList) {
        LambdaQueryWrapper<Major> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(Major::getId, idList);
        Major major = new Major();
        major.setDelFlag(true);
        return this.update(major, lambdaQueryWrapper);
    }
}
