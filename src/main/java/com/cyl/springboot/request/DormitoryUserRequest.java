package com.cyl.springboot.request;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class DormitoryUserRequest extends PageRequest {

    //宿舍id
    private String dormitoryId;

    //用户id
    private String userId;

    //用户ids
    private List<String> userIdList;


}
