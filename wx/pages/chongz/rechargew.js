// pages/chongz/recharge.js
var app = getApp()
var domain = app.globalData.domain;
Page({

  data: {
    activeIndex: 0, //默认选中第一个
    remind:'',
    numArray: [20, 30, 50, 80, 100,'m'],
    money:0,
    wfare:0,
    efare:0,
   
  },
 
  


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   //this.check();

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.check();
    const fareInfo = wx.getStorageSync('fareinformation');
    this.setData({
      money:fareInfo.waterfare
    })
    
   
    

  },
  check: function(){
    var _this = this;
    _this.setData({ remind: '加载中' });
    const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
    wx.request({
      url: `http://localhost:8080/dor/servlet/FareServlet`,
      data:{
        dorid:edusysUserInfo.dor_id,
        type:"1",
        userFrom: 'wechat',
        openid: app.globalData.openid,
       

      },
      timeout: app.globalData.requestTimeout,
      header: {
        'content-type': 'application/json' // 默认值
      },
      method: 'GET',
     
      success: function (res) {
        // console.log('eduSysProfile：', res.data)
        try {
          if (res.data.state=="true") {
            //res.data.password = password
            wx.setStorage({ data: res.data, key: 'fareinformation' })
            app.globalData.edusysUserInfo = res.data
            wx.vibrateShort({ type: 'medium' })
            wfare:res.data.wf
            efare: res.data.ef
            //wx.switchTab({ url: './index' })
          }
        } catch (error) {
          // _this.getCookie();
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 5000
          })
          _this.setData({ remind: '' });
        }
      }
    })
  },
  gowater: function () {
      // this.getCookie();
      wx.navigateTo({
        url: "rechargew"
      })
    

  },
  goindex:function(){
    wx.reLaunch({
      url: '../index/index',
    })
  },
  
  goelectricty: function () {
    // this.getCookie();
    wx.navigateTo({
      url: "rechargee"
    })


  },
  chongzhi: function () {
    // this.getCookie();
    var t=0;
    
    t = Number(this.data.money) + Number(this.data.numArray[this.data.activeIndex]);
    this.setData({
      money:  t
    })
    var _fare = wx.getStorageSync("fareinformation");
    const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
    _fare.waterfare = this.data.money;
    wx.setStorageSync('fareinformation', _fare);
    const fare = wx.getStorageSync("fareinformation");
    wx.request({
      url: `http://localhost:8080/dor/servlet/FareServlet`,
      data: {

        type: "3",
        dorid: edusysUserInfo.dor_id,
        waterfare: fare.waterfare,
        electrictyfare: fare.electrictyfare


      },
      timeout: app.globalData.requestTimeout,
      header: {
        'content-type': 'application/json' // 默认值
      },
      method: 'GET',

      success: function (res) {
        // console.log('eduSysProfile：', res.data)
        try {
          if (res.data.state == "true") {
            //res.data.password = password
            wx.setStorage({ data: res.data, key: 'fareinformation' })
            app.globalData.edusysUserInfo = res.data
            wx.vibrateShort({ type: 'medium' })
            wfare: res.data.wf
            efare: res.data.ef
            //wx.switchTab({ url: './index' })
          }
        } catch (error) {
          // _this.getCookie();
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 5000
          })
          _this.setData({ remind: '' });
        }
      }
    })


  },




  

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
   activethis: function (event) { //点击选中事件
    var thisindex = event.currentTarget.dataset.thisindex; //当前index
    this.setData({
      activeIndex: thisindex
    })
    },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
})