package com.cyl.springboot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyl.springboot.entity.Dormitory;
import com.cyl.springboot.entity.Notice;

public interface NoticeMapper extends BaseMapper<Notice> {
}
