package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("dormitory_user")
public class DormitoryUser {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    //宿舍id
    private String dormitoryId;

    //用户id
    private String userId;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
