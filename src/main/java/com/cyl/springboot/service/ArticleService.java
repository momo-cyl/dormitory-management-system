package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Article;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface ArticleService extends IService<Article> {

}
