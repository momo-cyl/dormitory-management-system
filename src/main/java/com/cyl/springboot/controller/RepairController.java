package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.common.Result;
import com.cyl.springboot.entity.Repair;
import com.cyl.springboot.entity.ServiceFile;
import com.cyl.springboot.enums.StatusEnum;
import com.cyl.springboot.request.RepairDeleteRequest;
import com.cyl.springboot.request.RepairRequest;
import com.cyl.springboot.request.RepairSaveRequest;
import com.cyl.springboot.response.RepairResponse;
import com.cyl.springboot.service.RepairService;
import com.cyl.springboot.service.ServiceFileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/repair")
public class RepairController {

    @Resource
    private RepairService repairService;

    @Resource
    private ServiceFileService serviceFileService;

    /**
     * 根据ID查询站点信息
     *
     * @param id 主键
     * @return
     */
    @GetMapping("/get/{id}")
    public Repair getRepair(@PathVariable("id") Integer id) {
        return repairService.getById(id);
    }

    /**
     * 列表查询
     *
     * @param request
     * @return
     */
    @PostMapping("/queryPage")
    public Page<RepairResponse> queryPage(@RequestBody RepairRequest request) {
        return repairService.queryPage(request);
    }


    /**
     * 新增或更新
     *
     * @param repair 站点实体
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody RepairSaveRequest repair) {
        repair.setStatus(StatusEnum.UN_PROCESSED.getDescription());
        if (!repairService.save(repair)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 更新
     *
     * @param repair
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody RepairSaveRequest repair) {
        if (!repairService.update(repair)) {
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 更新状态
     *
     * @param repair
     * @return
     */
    @PostMapping("/updateStatus")
    public Result updateStatus(@RequestBody Repair repair) {
        if (!repairService.updateById(repair)) {
            return Result.error();
        }
        return Result.success();
    }


    /**
     * 根据主键ID删除
     *
     * @param request 主键ID
     * @return
     */
    @PostMapping("/delete")
    public Result deleteRepair(@RequestBody RepairDeleteRequest request) {
        if (!repairService.removeById(request.getId())) {
            return Result.error();
        }
        LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ServiceFile::getServiceId, request.getRepairId());
        serviceFileService.remove(lambdaQueryWrapper);
        return Result.success();
    }

    /**
     * 根据主键集合批量删除
     *
     * @param request 主键集合
     * @return
     */
    @PostMapping("/delete/batch")
    public Result deleteBatch(@RequestBody RepairDeleteRequest request) {
        repairService.removeByIds(request.getIds());
        LambdaQueryWrapper<ServiceFile> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(ServiceFile::getServiceId, request.getRepairIdList());
        serviceFileService.remove(lambdaQueryWrapper);
        return Result.success();
    }

}
