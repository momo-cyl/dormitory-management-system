package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Comment;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
public interface CommentService extends IService<Comment> {

    List<Comment> findCommentDetail(Integer articleId);
}
