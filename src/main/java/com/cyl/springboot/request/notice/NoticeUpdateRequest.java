package com.cyl.springboot.request.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class NoticeUpdateRequest {

    //主键id
    private Long id;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("内容")
    private String content;

}
