package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 附件表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("service_file")
public class ServiceFile {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    //文件id
    private String fileId;

    //业务id
    private String serviceId;

}
