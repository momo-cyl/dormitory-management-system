package com.cyl.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cyl.springboot.entity.Notice;
import com.cyl.springboot.request.notice.NoticeRequest;
import com.cyl.springboot.request.notice.NoticeSaveRequest;
import com.cyl.springboot.request.notice.NoticeUpdateRequest;
import com.cyl.springboot.service.NoticeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/noticeManage")
public class NoticeManageController {

    @Resource
    private NoticeService noticeService;

    @GetMapping("/get/{id}")
    public Notice getById(@PathVariable("id") Long id) {
        return noticeService.getById(id);
    }

    @PostMapping("/queryPage")
    @ResponseBody
    public Page<Notice> queryPage(@RequestBody NoticeRequest noticeRequest) {
        return noticeService.queryPage(noticeRequest);
    }

    @PostMapping("/save")
    public Boolean save(@RequestBody NoticeSaveRequest noticeSaveRequest) {
        return noticeService.save(noticeSaveRequest);
    }

    @PostMapping("/update")
    public Boolean update(@RequestBody NoticeUpdateRequest noticeUpdateRequest) {
        return noticeService.update(noticeUpdateRequest);
    }

    @PostMapping("/delete")
    public Boolean delete(@RequestBody NoticeRequest noticeRequest) {
        return noticeService.deleteByIdList(noticeRequest.getIdList());
    }
}
