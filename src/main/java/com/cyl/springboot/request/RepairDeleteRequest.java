package com.cyl.springboot.request;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @version [版本号, 2023/2/20]
 */
@Data
public class RepairDeleteRequest {

    //主键id
    private Integer id;

    //主键ids
    private List<Integer> ids;

    //业务主键id
    private String repairId;

    //业务主键id
    private List<Integer> repairIdList;
}
