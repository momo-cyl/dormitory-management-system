//login.js
//获取应用实例
var app = getApp();
var domain = app.globalData.domain;
Page({
	data: {
		remind: '加载中',
		help_status: false,
		reset_status: false,
		userid_focus: false,
		passwd_focus: false,
		idcard_focus: false,
		userid: '',
		passwd: '',
		idcard: '',
		angle: 0,
		canIUseGetUserProfile: false
	},
	onLoad: function() {
		if (wx.getUserProfile) {
			this.setData({
				canIUseGetUserProfile: true
			})
		}
	},
	onReady: function() {
		try {
			const edusysUserInfo = wx.getStorageSync('edusysUserInfo');
			if (edusysUserInfo.name.length > 0) {
				wx.switchTab({
					url: './index',
				})
			}
		} catch (error) {

		}
		var _this = this;
		setTimeout(function() {
			_this.setData({
				remind: ''
			});
		}, 1000);
		wx.onAccelerometerChange(function(res) {
			var angle = -(res.x * 30).toFixed(1);
			if (angle > 14) {
				angle = 14;
			} else if (angle < -14) {
				angle = -14;
			}
			if (_this.data.angle !== angle) {
				_this.setData({
					angle: angle
				});
			}
		});
	},
	bind: function() {
		if (!this.vaildForm()) {
			return
		}
		var _this = this;
		wx.getUserProfile({
			lang: 'zh_CN',
			desc: '需要获取信息用来生成成绩海报',
			success: function(res) {
				wx.setStorage({
					data: res.userInfo,
					key: 'userInfo'
				});
				app.globalData.userInfo = res.userInfo
				_this.login(res.userInfo);
			},
			fail: function(e) {
				console.log(e)
				wx.showToast({
					title: '未授权将无法使用',
					icon: 'none'
				})
			}
		})
	},
	getUserInfo: function(e) {
		if (!this.vaildForm()) {
			return
		}
		this.setData({
			userInfo: e.detail.userInfo,
		})
		wx.setStorage({
			data: e.detail.userInfo,
			key: 'userInfo'
		});
		app.globalData.userInfo = e.detail.userInfo
		this.login(e.detail.userInfo);
	},
	vaildForm: function() {
		var username = this.data.userid;
		var password = this.data.passwd;
		if (username.length < 1) {
			wx.showToast({
				title: '请输入用户名',
				icon: 'none'
			})
			return false;
		}
		if (password.length < 1) {
			wx.showToast({
				title: '请输入密码',
				icon: 'none'
			})
			return false;
		}
		return true;
	},
	login: function(userInfo) {
		var _this = this;
		_this.setData({
			remind: '加载中'
		});
		var uid = this.data.userid;
		var password = this.data.passwd;
		wx.request({
			url: `http://114.116.117.208:9090/user/login`,
			data: {
				username: uid,
				password: password,
			},
			method: 'POST',
			success: function(res) {
				console.log(res);
				if (res.data.code === '200') {
					wx.setStorage({
						data: res.data.data,
						key: 'edusysUserInfo'
					})
					app.globalData.edusysUserInfo = res.data.data
					wx.vibrateShort({
						type: 'medium'
					})
					wx.switchTab({
						url: './index'
					})
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					})
					_this.setData({
						remind: ''
					});
				}
			}
		})
	},
	useridInput: function(e) {
		this.setData({
			userid: e.detail.value
		});
		if (e.detail.value.length >= 15) {
			wx.hideKeyboard();
		}
	},
	passwdInput: function(e) {
		this.setData({
			passwd: e.detail.value
		});
	},

	idcardInput: function(e) {
		this.setData({
			idcard: e.detail.value
		});
	},
	inputFocus: function(e) {
		if (e.target.id == 'userid') {
			this.setData({
				'userid_focus': true
			});
		} else if (e.target.id == 'passwd') {
			this.setData({
				'passwd_focus': true
			});
		} else if (e.target.id == 'idcard') {
			this.setData({
				'idcard_focus': true
			});
		}
	},
	inputBlur: function(e) {
		if (e.target.id == 'userid') {
			this.setData({
				'userid_focus': false
			});
		} else if (e.target.id == 'passwd') {
			this.setData({
				'passwd_focus': false
			});
		} else if (e.target.id == 'idcard') {
			this.setData({
				'idcard_focus': false
			});
		}
	}
});
