package com.cyl.springboot.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.College;
import com.cyl.springboot.mapper.CollegeMapper;
import com.cyl.springboot.request.college.CollegeRequest;
import com.cyl.springboot.request.college.CollegeSaveRequest;
import com.cyl.springboot.request.college.CollegeUpdateRequest;
import com.cyl.springboot.service.CollegeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class CollegeServiceImpl extends ServiceImpl<CollegeMapper, College> implements CollegeService {

    @Override
    public College getById(Long id) {
        LambdaQueryWrapper<College> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(College::getId, id);
        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<College> queryPage(CollegeRequest collegeRequest) {
        Page<College> page = new Page<>(collegeRequest.getPageNo(), collegeRequest.getPageSize());
        LambdaQueryWrapper<College> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(College::getDelFlag, false);
        if (StrUtil.isNotBlank(collegeRequest.getNameLike())) {
            lambdaQueryWrapper.like(College::getName, collegeRequest.getNameLike());
        }
        lambdaQueryWrapper.orderByDesc(College::getCreateTime);
        return this.page(page, lambdaQueryWrapper);
    }

    @Override
    public Boolean save(CollegeSaveRequest collegeSaveRequest) {
        College college = new College();
        college.setName(collegeSaveRequest.getName());
        return this.save(college);
    }

    @Override
    public Boolean update(CollegeUpdateRequest collegeUpdateRequest) {
        LambdaQueryWrapper<College> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(College::getId, collegeUpdateRequest.getId());
        College college = new College();
        college.setName(collegeUpdateRequest.getName());
        return this.update(college, lambdaQueryWrapper);
    }

    @Override
    public Boolean deleteByIdList(List<Long> idList) {
        LambdaQueryWrapper<College> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(College::getId, idList);
        College college = new College();
        college.setDelFlag(true);
        return this.update(college, lambdaQueryWrapper);
    }
}
