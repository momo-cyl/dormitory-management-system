package com.cyl.springboot.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cyl.springboot.entity.Notice;
import com.cyl.springboot.mapper.NoticeMapper;
import com.cyl.springboot.request.notice.NoticeRequest;
import com.cyl.springboot.request.notice.NoticeSaveRequest;
import com.cyl.springboot.request.notice.NoticeUpdateRequest;
import com.cyl.springboot.service.NoticeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author cyl
 * @since 2022-03-22
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Override
    public Notice getById(Long id) {
        LambdaQueryWrapper<Notice> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Notice::getId, id);
        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<Notice> queryPage(NoticeRequest noticeRequest) {
        Page<Notice> page = new Page<>(noticeRequest.getPageNo(), noticeRequest.getPageSize());
        LambdaQueryWrapper<Notice> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Notice::getDelFlag, false);
        if (StrUtil.isNotBlank(noticeRequest.getTitleLike())) {
            lambdaQueryWrapper.like(Notice::getTitle, noticeRequest.getTitleLike());
        }
        lambdaQueryWrapper.orderByDesc(Notice::getCreateTime);
        return this.page(page, lambdaQueryWrapper);
    }

    @Override
    public Boolean save(NoticeSaveRequest noticeSaveRequest) {
        Notice notice = new Notice();
        notice.setTitle(noticeSaveRequest.getTitle());
        notice.setContent(noticeSaveRequest.getContent());
        return this.save(notice);
    }

    @Override
    public Boolean update(NoticeUpdateRequest noticeUpdateRequest) {
        LambdaQueryWrapper<Notice> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Notice::getId, noticeUpdateRequest.getId());
        Notice notice = new Notice();
        notice.setTitle(noticeUpdateRequest.getTitle());
        notice.setContent(noticeUpdateRequest.getContent());
        return this.update(notice, lambdaQueryWrapper);
    }

    @Override
    public Boolean deleteByIdList(List<Long> idList) {
        LambdaQueryWrapper<Notice> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(Notice::getId, idList);
        Notice notice = new Notice();
        notice.setDelFlag(true);
        return this.update(notice, lambdaQueryWrapper);
    }
}
