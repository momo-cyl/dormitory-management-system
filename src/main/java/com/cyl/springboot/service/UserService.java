package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.dto.UserDTO;
import com.cyl.springboot.dto.UserPasswordDTO;
import com.cyl.springboot.entity.User;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author cyl
 * @since 2022-01-26
 */
public interface UserService extends IService<User> {

    UserDTO login(UserDTO userDTO);

    User register(UserDTO userDTO);

    void updatePassword(UserPasswordDTO userPasswordDTO);

    Page<User> findPage(Page<User> objectPage, String username, String email, String address);

    List<User> getList(List<String> idList);

}
