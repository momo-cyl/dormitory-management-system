package com.cyl.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 宿舍表
 *
 * @author cyl
 * @version [1.0, 17:26 2023/2/20]
 */
@Data
@TableName("location")
public class Location {

    //ID
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String userId;

    private String address;

    private BigDecimal latitude;

    private BigDecimal longitude;

    @JsonFormat(timezone = "GMT+8")
    private LocalDateTime signInTime;

    //创建时间
    private LocalDate createTime;

    //更新时间
    private LocalDate updateTime;

}
