package com.cyl.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.College;
import com.cyl.springboot.request.college.CollegeRequest;
import com.cyl.springboot.request.college.CollegeSaveRequest;
import com.cyl.springboot.request.college.CollegeUpdateRequest;

import java.util.List;


public interface CollegeService extends IService<College> {

    College getById(Long id);

    Page<College> queryPage(CollegeRequest collegeRequest);

    Boolean save(CollegeSaveRequest collegeSaveRequest);

    Boolean update(CollegeUpdateRequest collegeUpdateRequest);

    Boolean deleteByIdList(List<Long> idList);

}
