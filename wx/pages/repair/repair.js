Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		info: {}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function(options) {
		let that = this;
		let urlList = [];
		let fileIdList = [];
		console.log(options);
		let result = {};
		result.id = options.id;
		result.repairId = options.repairId;
		if (options.urlList.split(",") != "null") {
			urlList = options.urlList.split(",");
		}
		if (options.fileIdList.split(",") != "") {
			fileIdList = options.fileIdList.split(",");
		}
		if (options.dormitoryRoom != "null") {
			result.dormitoryRoom = options.dormitoryRoom;
		}
		if (options.userId != "null") {
			result.userId = options.userId;
		}
		if (options.userName != "null") {
			result.userName = options.userName;
		}
		if (options.dormitoryId != "null") {
			result.dormitoryId = options.dormitoryId;
		}
		if (options.status != "null") {
			result.status = options.status;
			if(options.status == "已处理"){
				result.handleStatus = true;
			}else{
				result.handleStatus = false;
			}
		}
		if (options.description != "null") {
			result.description = options.description;
		}
		result.urlList = urlList;
		result.fileIdList = fileIdList;
		console.log(result)
		wx.request({
			url: `http://114.116.117.208:9090/dormitory/getAll/`,
			method: 'POST',
			success: function(res) {
				console.log(res);
				result.dormitoryList = res.data;
				if (res.data != null) {
					let arr = [];
					for (var i = 0; i < res.data.length; i++) {
						arr.push(res.data[i].dormitoryBuilding + res.data[i].dormitoryNumber)
					}
					result.dormitoryShow = arr;
				}
				wx.request({
					url: `http://114.116.117.208:9090/dormitoryUser/getDormitoryUserList`,
					method: 'POST',
					data: {
						dormitoryId: result.dormitoryId
					},
					success: function(res) {
						console.log(res);
						if (res.data != null) {
							let arr2 = [];
							let arr3 = [];
							for (var i = 0; i < res.data.length; i++) {
								if(res.data[i].status == "USING"){
									arr2.push(res.data[i].userName)
									arr3.push(res.data[i]);
								}
							}
							result.studentShow = arr2;
							result.studentList = arr3;
						}
						console.log(result);
						that.setData({
							info: result,
						})
					}
				})
			}
		})
	},
	bindKeyInput: function(e) {
		this.data.info[`${e.currentTarget.dataset.params}`] = e.detail.value
		this.setData({
			info: this.data.info
		})
		console.log(`info对象：`, this.data.info)
	},
	deleteImage(e) {
		let index = e.currentTarget.dataset.params;
		let arr = this.data.info.urlList;
		let arr2 = this.data.info.fileIdList;
		arr.splice(index, 1);
		arr2.splice(index, 1);
		this.data.info.urlList = arr;
		this.data.info.fileIdList = arr2;
		console.log(this.data.info);
		this.setData({
			info: this.data.info
		})
	},
	formSubmit: function(e) {
		const that = this;
		let request = this.data.info;
		if (request.id == null) {
			return;
		}
		if(request.dormitoryId == null || request.dormitoryId == ''){
			wx.showToast({
				title: '请选择宿舍信息',
				icon: 'none'
			})
			return;
		}
		if(request.userId == null || request.userId == ''){
			wx.showToast({
				title: '请选择学生信息',
				icon: 'none'
			})
			return;
		}
		
		console.log(request)
		wx.request({
			url: `http://114.116.117.208:9090/repair/update`,
			method: 'POST',
			data: {
				id: request.id,
				dormitoryId: request.dormitoryId,
				userId: request.userId,
				status: request.status,
				fileIdList: request.fileIdList,
				description: request.description,
				repairId: request.repairId
			},
			success: function(res) {
				if (res.data.code === '200') {
					console.log(11)
				} else {
					wx.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 5000
					});
				}
			}
		})
	},
	uploadPhoto(e) { // 拍摄或从相册选取上传
		let that = this;
		wx.chooseImage({
			count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success(res) {
				let tempFilePaths = res.tempFilePaths; // 返回选定照片的本地路径列表 
				that.upload(that, tempFilePaths);
			}
		})
	},
	upload(page, path) { // 上传图片
		let result = this.data.info;
		let that = this;
		wx.showToast({
			icon: "loading",
			title: "正在上传……"
		});
		wx.uploadFile({
			url: 'http://114.116.117.208:9090/file/saveFile', //后端接口
			filePath: path[0],
			name: 'file',
			header: {
				"Content-Type": "multipart/form-data"
			},
			success(res) {
				let str = res.data.replace(/\ufeff/g, "") // step1: 首先去掉两头的"",
				let data = JSON.parse(str) // step2: 转换成JSON格式的数据
				console.log(data);
				console.log(res);
				if (res.statusCode != 200) {
					wx.showModal({
						title: '提示',
						content: '上传失败',
						showCancel: false
					});
					return;
				} else {
					result.urlList.push(data.url);
					result.fileIdList.push(data.fileId);
					console.log(result);
					that.setData({
						info: result
					});
				}
			},
			fail(e) {
				wx.showModal({
					title: '提示',
					content: '上传失败',
					showCancel: false
				});
			},
			complete() {
				wx.hideToast(); //隐藏Toast
			}
		})
	},
	dormitoryChange(e) {
		let that = this;
		let data = this.data.info.dormitoryList[e.detail.value];
		let result = this.data.info;
		result.dormitoryId = data.dormitoryId;
		result.dormitoryRoom = data.dormitoryBuilding + data.dormitoryNumber;
		wx.request({
			url: `http://114.116.117.208:9090/dormitoryUser/getDormitoryUserList`,
			method: 'POST',
			data: {
				dormitoryId: data.dormitoryId
			},
			success: function(res) {
				console.log(res);
				if (res.data != null) {
					let arr2 = [];
					let arr3 = [];
					for (var i = 0; i < res.data.length; i++) {
						if(res.data[i].status == "USING"){
							arr2.push(res.data[i].userName)
							arr3.push(res.data[i]);
						}
					}
					result.studentList = arr3;
					if(arr2 == null || arr2.length ==0){
						result.userName = "";
					}
					result.studentShow = arr2;
				}else{
					result.studentShow = [];
					result.userName = null;
				}
				console.log(result);
				that.setData({
					info: result,
				})
			}
		})
	},
	studentChange(e) {
		console.log(this.data.info.studentList);
		console.log(e);
		let data = this.data.info.studentList[e.detail.value];
		let result = this.data.info;
		result.userId = data.userId;
		result.userName = data.userName;
		this.setData({
			info: result
		});
	},
	handleStatus(e) {
		console.log(e)
		let flag = e.detail.value;
		console.log(flag);
		let result = this.data.info;
		if(flag == false){
			result.status = "未处理"
			result.handleStatus = flag;
		}else{
			result.status = "已处理"
			result.handleStatus = flag;
		}
		this.setData({
			info: result
		});
		
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function() {

	}
})
