package com.cyl.springboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cyl.springboot.entity.Repair;
import com.cyl.springboot.request.RepairRequest;
import com.cyl.springboot.request.RepairSaveRequest;
import com.cyl.springboot.response.RepairResponse;

public interface RepairService extends IService<Repair> {

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    Page<RepairResponse> queryPage(RepairRequest request);

    /**
     * 保存
     *
     * @param request
     * @return
     */
    boolean save(RepairSaveRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    boolean update(RepairSaveRequest request);


}
